//
//  CustomerSupportListViewController.swift
//  SOSUDA PAY
//
//  Created by Saifur Rahman on 19/04/21.
//

import UIKit

class CustomerSupportListViewController: UIViewController {
    @IBOutlet weak var supportlistTableView: UITableView!
    @IBOutlet weak var topNavBar: TopHeader!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        topNavBar.title.text = "Customer Support List"
        topNavBar.title.textColor = .white
        topNavBar.backButton.setImage(UIImage(systemName: "chevron.backward"), for: .normal)
        topNavBar.backButton.tintColor = .white
        topNavBar.backButtonWidth.constant = 60
        // Do any additional setup after loading the view.
    }
    @IBAction func addTicket(_ sender: Any) {
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
