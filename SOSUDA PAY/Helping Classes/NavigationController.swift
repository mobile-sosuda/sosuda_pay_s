//
//  NavigationController.swift
//  Green Knuckles
//
//  Created by JAMTECH MAC 2 on 28/07/20.
//  Copyright © 2020 JAMTECH MAC 2. All rights reserved.
//

import UIKit

class NavigationController: UINavigationController {

    override var shouldAutorotate: Bool {
          return false
      }

      override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
          return .portrait
      }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
