//
//  APIManager.swift
//  InformedConsent
//
//  Created by Shivam Srivastava 09 on 23/05/20.
//  Copyright © 2020 Shivam Srivastava 09. All rights reserved.
//

import Foundation
import Alamofire

class ResponseFromApi  {
     static let shared = ResponseFromApi()
     private init()  {
       }
   
    typealias JSONTaskCompletionHandler = (Any)
    typealias isSuccess = (Bool)
    typealias data = (Data)
    
    //MARK: POST Method
    func postRequestForParameters(url:String, params:Dictionary<String,Any>,header : [String:String]? = nil, completion: @escaping(JSONTaskCompletionHandler,data?,isSuccess)-> () ){
        var header1 : HTTPHeaders! = nil
        if let _ = header{
            header1 = HTTPHeaders(header!)
        }else{
            header1 = nil
        }
    
        AF.request(url, method: .post, parameters: params, encoding: JSONEncoding.default, headers: header1).responseJSON { [weak self]
               response in
           
              switch response.result {
              case .success (let JSON):
                print(response.result)
                
                 let data = JSON as? Dictionary<String,Any>
                if data == nil{
                    completion(data ?? "",nil,false)
                }else{
                    completion(data ?? "",response.data,true)
                }
                   
                break
              case .failure(let error):
                completion(error,nil ,false)
                break
              }
           }
    }
    
    //MARK: POST Method
    func getRequestForParameters(url:String, completion: @escaping(JSONTaskCompletionHandler,isSuccess)-> () ){
        let req = AF.request(url)
        let img = UIImage()
        print(req.data)
    
        req.responseJSON { (response) in
            
               switch response.result {
               case .success (let JSON):
                 print(response.result)
                 
                  let data = JSON as? Dictionary<String,Any>
                 if data == nil{
                     completion(data ?? "",false)
                 }else{
                     completion(data ?? "",true)
                 }
                    
                 break
               case .failure(let error):
                 completion(error, false)
                 break
               }
        }
        
//        AF.request(url, method: .get, parameters: params, encoding: JSONEncoding.default, headers: nil).responseJSON { [weak self]
//               response in
//
//              switch response.result {
//              case .success (let JSON):
//                print(response.result)
//
//                 let data = JSON as? Dictionary<String,Any>
//                if data == nil{
//                    completion(data ?? "",false)
//                }else{
//                    completion(data ?? "",true)
//                }
//
//                break
//              case .failure(let error):
//                completion(error, false)
//                break
//              }
//           }
    }
    
    func postRequestWithHeader(url:String,header : [String:String]? = nil, completion: @escaping(JSONTaskCompletionHandler,data?,isSuccess)-> () ){
        var header1 : HTTPHeaders? = nil
        if let _ = header{
            header1 = HTTPHeaders(header!)
        }else{
            header1 = nil
        }
        
        AF.request(url, method: .post , encoding: JSONEncoding.default, headers: header1!).responseJSON { [weak self]
               response in
           
            
              switch response.result {
              case .success (let JSON):
                print(response.result)
                
                 let data = JSON as? Dictionary<String,Any>
                if data == nil{
                    completion(data ?? "",nil,false)
                }else{
                    completion(data ?? "",response.data,true)
                }
                   
                break
              case .failure(let error):
                completion(error,nil ,false)
                break
              }
           }
    }
    func getRequestWithHeader(url:String,header : [String:String]? = nil, completion: @escaping(JSONTaskCompletionHandler,data?,isSuccess)-> () ){
        var header1 : HTTPHeaders? = nil
        if let _ = header{
            header1 = HTTPHeaders(header!)
        }else{
            header1 = nil
        }
    
        AF.request(url, method: .get, encoding: JSONEncoding.default, headers: header1!).responseJSON { [weak self]
               response in
           
              switch response.result {
              case .success (let JSON):
                print(response.result)
                
                 let data = JSON as? Dictionary<String,Any>
                if data == nil{
                    completion(data ?? "",nil,false)
                }else{
                    completion(data ?? "",response.data,true)
                }
                   
                break
              case .failure(let error):
                completion(error,nil ,false)
                break
              }
           }
    }
    func upload(image: Data, to url: String, params: [String: Any], completion: @escaping(JSONTaskCompletionHandler,isSuccess)-> ()) {
        
        var request = URLRequest(url: URL(string: url)!)
        let headers: HTTPHeaders =
            ["Content-type": "multipart/form-data",
             "Accept": "application/json"]
        request.httpMethod = "POST"
        AF.upload(multipartFormData: { (multiPart) in
            for (key, value) in params {
                if let temp = value as? String {
                    multiPart.append(temp.data(using: .utf8)!, withName: key)
                }
                if let temp = value as? Int {
                    multiPart.append("\(temp)".data(using: .utf8)!, withName: key)
                }
                if let temp = value as? NSArray {
                    temp.forEach({ element in
                        let keyObj = key + "[]"
                        if let string = element as? String {
                            multiPart.append(string.data(using: .utf8)!, withName: keyObj)
                        } else
                            if let num = element as? Int {
                                let value = "\(num)"
                                multiPart.append(value.data(using: .utf8)!, withName: keyObj)
                        }
                    })
                }
            }
            
            
            multiPart.append(image, withName: "image_string", fileName: "file.png", mimeType: "image/png")
        }, to: url as URLConvertible, method: .post, headers: nil).uploadProgress(queue: .main, closure: { progress in
            //Current upload progress of file
            print("Upload Progress: \(progress.fractionCompleted)")
        })
            .responseString(completionHandler: { data in
                //Do what ever you want to do with response
                switch data.result {
                case .success (let JSON):
                    print(data.result)
                    let data = JSON as? Any
                    completion(data ?? "",true)
                    break
                case .failure(let error):
                    completion(error, false)
                    print(error)
                    break
                }
            })
        //        AF.upload(multipartFormData: { multiPart in
        //            for (key, value) in params {
        //                if let temp = value as? String {
        //                    multiPart.append(temp.data(using: .utf8)!, withName: key)
        //                }
        //                if let temp = value as? Int {
        //                    multiPart.append("\(temp)".data(using: .utf8)!, withName: key)
        //                }
        //                if let temp = value as? NSArray {
        //                    temp.forEach({ element in
        //                        let keyObj = key + "[]"
        //                        if let string = element as? String {
        //                            multiPart.append(string.data(using: .utf8)!, withName: keyObj)
        //                        } else
        //                            if let num = element as? Int {
        //                                let value = "\(num)"
        //                                multiPart.append(value.data(using: .utf8)!, withName: keyObj)
        //                        }
        //                    })
        //                }
        //            }
        //
        //
        //            multiPart.append(image, withName: "file", fileName: "file.png", mimeType: "image/png")
        //        }, with: url as URLConvertible)
        //            .uploadProgress(queue: .main, closure: { progress in
        //                //Current upload progress of file
        //                print("Upload Progress: \(progress.fractionCompleted)")
        //            })
        //            .responseJSON(completionHandler: { data in
        //                //Do what ever you want to do with response
        //                switch data.result {
        //                             case .success (let JSON):
        //                               print(data.result)
        //                                let data = JSON as? Dictionary<String,Any>
        //                                   completion(data ?? "",true)
        //                               break
        //                             case .failure(let error):
        //                               completion(error, false)
        //                               break
        //                             }
        //            })
    }
    
    func uploadMultimediaData(image: Data, url: String, params: Dictionary<String, Any>,completion: @escaping(JSONTaskCompletionHandler,isSuccess)-> ()) {
        let Surl = URL(string: url)
        AF.upload(multipartFormData: { multiPart in
            for (key, value) in params {
                if let temp = value as? String {
                    multiPart.append(temp.data(using: .utf8)!, withName: key)
                }
                if let temp = value as? Int {
                    multiPart.append("\(temp)".data(using: .utf8)!, withName: key)
                }
                if let temp = value as? NSArray {
                    temp.forEach({ element in
                        let keyObj = key + "[]"
                        if let string = element as? String {
                            multiPart.append(string.data(using: .utf8)!, withName: keyObj)
                        } else
                            if let num = element as? Int {
                                let value = "\(num)"
                                multiPart.append(value.data(using: .utf8)!, withName: keyObj)
                        }
                    })
                }
            }
            multiPart.append(image, withName: "image_string", fileName: "file.png", mimeType: "image/png")
        },  to: Surl!,
               method: .post,
               headers: nil)
            .uploadProgress(queue: .main, closure: { progress in
                //Current upload progress of file
                print("Upload Progress: \(progress.fractionCompleted)")
            })
            .responseJSON(completionHandler: { data in
                switch data.result {
                case .success (let JSON):
                    print(data.result)
                    let data1 = JSON as? Dictionary<String,Any>
                    completion(data1 ?? "",true)
                    break
                case .failure(let error):
                    completion(error, false)
                    break
                }
                
            })
    }
}
