//
//  URLManager.swift
//  InformedConsent
//
//  Created by Shivam Srivastava 09 on 23/05/20.
//  Copyright © 2020 Shivam Srivastava 09. All rights reserved.
//

import Foundation
import UIKit
let Defaults = UserDefaults.standard
//otp/RequestOTP/%7bcountryCode+MobilenNo%7d
let baseURL = "http://119.82.68.85:81/SOSUDAPAYAPI/api/Sosudapay/"
let verifyOTP = baseURL+"otp/VerifyOTP/"
let sendOtp = baseURL+"otp/RequestOTP/"
let ACCOUNTBAL = baseURL + "AccountBalance"
let VERIFYUSERID = baseURL + "Users/GetUser" 
let STRIPEADDCARD = baseURL + "StripeCard/AddStripeCardAsync"
let STRIPEGETCARDS = baseURL + "StripeCard/GetCardListAsync"
let DELETESTRIPECARD = baseURL + "StripeCard/DeleteCardAsync"
let TRANSACTION_HISTORY = baseURL + "TransactionHistory"
let ADDMONEY = baseURL + "StripeCharge/ChargeAsync"
let GET_TRANSACTION_CONVERSION = baseURL + "UserSubscription/GetUserTransactionConversion"
let P2P_TRANSACTION = baseURL + "P2PTransaction"
let getCountryList = baseURL+"CountryMaster/GetCountryList"
let termCondition = baseURL+"/mobileapi/v1/get_term_conditions"
let forgetPassword = baseURL+"Otp/RequestPasswordOTP/"
let changeforgettenPassword = baseURL+"Users/Forgotpassword"
let changePassword = baseURL+"/mobileapi/v1/changePassword"
let editProfile = baseURL+"/mobileapi/v1/update_profile"
let editProfileImage = baseURL+"/mobileapi/v1/update_profile_image"
let getQuestions = baseURL+"/mobileapi/v1/get_patient_form"
let checkUserNameAPI = baseURL+"/mobileapi/v1/check_user_name"
let registerUserAPI = baseURL+"Users/register"
let loginAPI = baseURL+"Users/Authenticate"
let setReactionTime = baseURL+"/mobileapi/v1/create_reaction_time"
let getReactionResult = baseURL+"/mobileapi/v1/get_reaction_avg_time"
let setVariables = baseURL+"/mobileapi/v1/set_varriables"
let getVariables = baseURL+"/mobileapi/v1/get_varriables"
let emailCheckAPI = baseURL+"/mobileapi/v1/check_email"
let updatePatientAPI = baseURL+"/mobileapi/v1/update_patient"
let faqAPI = baseURL+"/mobileapi/v1/get_faqs"
let getMyProfile = baseURL + "/mobileapi/v1/getProfile"
let setVisionValues = baseURL + "/mobileapi/v1/create_vision_time"
let getVisionValues = baseURL + "/mobileapi/v1/get_vision_data"
let removeVisionValues = baseURL + "/mobileapi/v1/remove_vision_data"
let removeReactionTime = baseURL + "/mobileapi/v1/remove_reaction_time"
let getFlagImage = "http://119.82.68.85:81/SOSUDAPAYAPI"


//1. Set Vision Data:

//    url: https://greenknuckles.betaplanets.com/wp-json/mobileapi/v1/getProfile
//
//    Parameters:
//    token
//    item
//    time_taken
//
//2. Get Vision Data:
//
//    url: https://greenknuckles.betaplanets.com/wp-json/mobileapi/v1/get_vision_data
//
//    Parameters:
//    token
//
//3. Remove Vision Data:
//    url: https://greenknuckles.betaplanets.com/wp-json/mobileapi/v1/remove_vision_data
//
//    Parameters:
//    vision_data_id
//
//
