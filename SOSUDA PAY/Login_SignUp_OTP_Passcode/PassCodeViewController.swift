//
//  PassCodeViewController.swift
//  SOSUDA PAY
//
//  Created by Saifur Rahman on 07/03/21.
//

import UIKit
import LinearProgressBar
class PassCodeViewController: UIViewController {
var val = CGFloat(0)
    @IBOutlet weak var codeTextfield: UITextField!
    @IBOutlet weak var pBar: LinearProgressBar!
    @IBOutlet weak var createPasscodeHeaderLabel: UILabel!
    @IBOutlet weak var proceedbtn: DesignableButton!
    var codeCreatedNowNeedToConfirm  = Bool()
    var createdPasscode = String()
    override func viewDidLoad() {
        super.viewDidLoad()
        proceedbtn.isHidden = true
        pBar.layer.cornerRadius = 10
        pBar.progressValue = val
        codeTextfield.delegate  = self
        //codeTextfield.becomeFirstResponder()
        if !createdPasscode.isEmpty{
            val =  0
            pBar.progressValue = val * CGFloat(25)
        codeCreatedNowNeedToConfirm = true
        codeTextfield.text?.removeAll()
        UILabel.animate(withDuration: 0.5) {
            self.createPasscodeHeaderLabel.text = "Confirm Passcode"
            self.proceedbtn.isHidden = false
        }
        }
        self.view.endEditing(true)
        // Do any additional setup after loading the view.
    }
    
    @IBAction func numberCLicked(_ sender: UIButton) {
        if codeTextfield.text!.count < 4{
        codeTextfield.text = codeTextfield.text! + "\(sender.tag)"
        val +=  1
        pBar.progressValue = val * CGFloat(25)
            if codeTextfield.text!.count == 4{
            if codeCreatedNowNeedToConfirm != true{
                    self.createdPasscode = self.codeTextfield.text!
                    val =  0
                    pBar.progressValue = val * CGFloat(25)
                codeCreatedNowNeedToConfirm = true
                codeTextfield.text?.removeAll()
                UILabel.animate(withDuration: 0.5) {
                    self.createPasscodeHeaderLabel.text = "Confirm Passcode"
                    self.proceedbtn.isHidden = false
                }
                }else{
                   
                    
                }
               
               
            }
        }
        //val = val  CGFloat(25)
    }
    
    @IBAction func clearButton(_ sender: Any) {
        if  !codeTextfield.text!.isEmpty{
            let str = codeTextfield.text!
            let c = str.lastIndex(of: str.last!)
            codeTextfield.text?.remove(at:c!)
            val -=  1
            pBar.progressValue = val * CGFloat(25)
        }
    }
    @IBAction func send(_ sender: Any) {
        if codeTextfield.text!.count == 4{
            if createdPasscode == self.codeTextfield.text! {
                UserDefaults.standard.setValue(codeTextfield.text!, forKey: "PassCode")
                DispatchQueue.main.async {
                    let viewCOntroller = self.storyboard?.instantiateViewController(withIdentifier: "MainTabBarViewController") as? MainTabBarViewController
               
                    self.navigationController?.pushViewController(viewCOntroller!, animated: true)
                }
            }else{
                Alert.showBasic(title: "Error", message: "Passcode doesn't match", vc: self)
            }
         
        }else{
            Alert.showBasic(title: "Error", message: "Passcode should be of 4 digits", vc: self)
        }
//        pBar.progressValue = val + CGFloat(25)
//        val = val + CGFloat(25)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension PassCodeViewController:UITextFieldDelegate{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let (shouldReturn,count) = textField.maxLenghtIntextfield(length: 4, range: range, string: string)
        pBar.progressValue = CGFloat(25*count)
      return shouldReturn
    }
}
