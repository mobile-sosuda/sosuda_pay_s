//
//  EnterSignUpDetailsViewController.swift
//  SOSUDA PAY
//
//  Created by Saifur Rahman on 08/03/21.
//

import UIKit

class EnterSignUpDetailsViewController: UIViewController {
    var phoneNumber : String? = "9564564556"
    var selectedDic = NSDictionary()
    @IBOutlet weak var firstNmaeHeader: UILabel!
    @IBOutlet weak var lastNameHeader: UILabel!
    @IBOutlet weak var confirmPasswordHeader: UILabel!
    @IBOutlet weak var passwordHeader: UILabel!
    @IBOutlet weak var mobileNumberHeader: UILabel!
    @IBOutlet weak var topHeaderView: TopHeader!
    @IBOutlet weak var passwordTf: Designabletextfield!
    @IBOutlet weak var mobileNumTf: Designabletextfield!
    @IBOutlet weak var confirmPassTf: Designabletextfield!
    @IBOutlet weak var lastNameTf: Designabletextfield!
    @IBOutlet weak var firstNameTf: Designabletextfield!
    override func viewDidLoad() {
        super.viewDidLoad()
        setupToHideKeyboardOnTapOnView()
        mobileNumTf.text = phoneNumber!
        topHeaderView.title.text = "User Registration"
        passwordTf.delegate = self
        lastNameTf.delegate = self
        firstNameTf.delegate = self
        confirmPassTf.delegate = self
        firstNameTf.tag = 2
        lastNameTf.tag = 3
        passwordTf.tag = 4
        confirmPassTf.tag = 5
    }
    
    @IBAction func confirmDetails(_ sender: Any) {
        if validate(){
           // Alert.showBasic(title: "fdg", message: "true", vc: self)
            let deviceId = UIDevice.current.identifierForVendor!.uuidString

            let param = ["mobileNumber": mobileNumTf.text!,
                         "password": passwordTf.text!.trimmingCharacters(in: .whitespaces),
                         "firstName": firstNameTf.text!.trimmingCharacters(in: .whitespaces),
            "lastName": lastNameTf.text!.trimmingCharacters(in: .whitespaces),
            "countryAlphaCode": selectedDic["alpha3Code"] as! String, //Return fron Get Country API
            "callingCode": selectedDic["callingCodes"] as! String, //Return fron Get Country API as
            "deviceId": deviceId, // Mobile Phone Device Id
            "deviceType": "iOS", // Android/iOS/Web
            "fcmToken": "iOSDhjfcmknhf", // FCM Token for push notifiction
            "subscriptionId": 1] as Dictionary<String,Any>
            ResponseFromApi.shared.postRequestForParameters(url: registerUserAPI, params: param) { (resp,dat, sucessIs) in
                if sucessIs{  if let msg = (resp as! NSDictionary)["message"] as? String{
                    Alert.showBasic(title: "Already registered", message: msg, vc: self)
                }else {
                        DispatchQueue.main.async {
                            let viewCOntroller = self.storyboard?.instantiateViewController(withIdentifier: "PassCodeViewController") as? PassCodeViewController
                       
                            self.navigationController?.pushViewController(viewCOntroller!, animated: true)
                        }
                    }
                    
                }
            }
        }
    }
    
    func validate()-> Bool{
        if (firstNameTf.text?.trimmingCharacters(in: .whitespaces).isEmpty)!{
           
            Alert.showBasic(title: "EMpty credential", message: "Please enter a FIrst Name", vc: self)
            return false
        }else if (lastNameTf.text?.trimmingCharacters(in: .whitespaces).isEmpty)!{
            
            Alert.showBasic(title: "Empty credential", message: "Please enter a Last Name", vc: self)
            return false
        }else if !(passwordTf.text?.trimmingCharacters(in: .whitespaces).isValidPassword())!{
            
            Alert.showBasic(title: "Empty credential", message: "Enter a valid password", vc: self)
            return false
        }else if !(confirmPassTf.text?.trimmingCharacters(in: .whitespaces).isValidPassword())!{
            
            Alert.showBasic(title: "Empty credential", message: "Enter a valid confirm password", vc: self)
            return false
        }else if (passwordTf.text?.trimmingCharacters(in: .whitespaces))! != (confirmPassTf.text?.trimmingCharacters(in: .whitespaces))!{
            
            Alert.showBasic(title: "Error", message: "Password Does not match", vc: self)
            return false
        }else{
            return true
        }
    }

    // MARK: - Password Valid
    
}
extension EnterSignUpDetailsViewController:UITextFieldDelegate{
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField.tag == 2{
            firstNmaeHeader.isHidden = false
            firstNameTf.placeholder = ""
        } else if textField.tag == 3{
            lastNameHeader.isHidden = false
            lastNameTf.placeholder = ""
        } else if textField.tag == 4{
            passwordHeader.isHidden = false
            passwordTf.placeholder = ""
        } else if textField.tag == 5{
            confirmPasswordHeader.isHidden = false
            confirmPassTf.placeholder = ""

        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField.tag == 2{
            firstNmaeHeader.isHidden = true
            firstNameTf.placeholder = "First Name"
        } else if textField.tag == 3{
            lastNameHeader.isHidden = true
            lastNameTf.placeholder = "Last Name"
        } else if textField.tag == 4{
            passwordHeader.isHidden = true
            passwordTf.placeholder = "Password"
        } else if textField.tag == 5{
            confirmPasswordHeader.isHidden = true
            confirmPassTf.placeholder = "Confirm Password"
        }

    }
}
