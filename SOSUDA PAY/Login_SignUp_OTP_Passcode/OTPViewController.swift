//
//  OTPViewController.swift
//  SOSUDA PAY
//
//  Created by Saifur Rahman on 07/03/21.
//

import UIKit

class OTPViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var phoneLabel: UILabel!
    var dialCode :String?
    var selectedDic = NSDictionary()
    @IBOutlet weak var topHeader: TopHeader!
    @IBOutlet weak var tf1: UITextField!
    @IBOutlet weak var tf2: UITextField!
    @IBOutlet weak var tf3: UITextField!
    @IBOutlet weak var tf4: UITextField!
    var phoneNumber :String?
    override func viewDidLoad() {
        super.viewDidLoad()
        setupToHideKeyboardOnTapOnView()
        topHeader.backButtonWidth.constant = 0
        topHeader.title.text = "User Registration"
        if let _ = phoneNumber{
        phoneLabel.text = phoneNumber!
        }
        tf1.becomeFirstResponder()
        tf1.delegate = self
        tf2.delegate = self
        tf3.delegate = self
        tf4.delegate = self
        tf1.tag = 1
        tf2.tag = 2
        tf3.tag = 3
        tf4.tag = 4
        tf1.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: UIControl.Event.editingChanged)
        tf2.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: UIControl.Event.editingChanged)
        tf3.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: UIControl.Event.editingChanged)
        tf4.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: UIControl.Event.editingChanged)
           }
    
                // Do any additional setup after loading the view.
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if !textField.text!.isEmpty && string.count == 1{
            if textField.tag == 1{
                tf2.becomeFirstResponder()
            }else if textField.tag == 2{
                tf3.becomeFirstResponder()

            }else if textField.tag == 3{
                tf4.becomeFirstResponder()

            }else{
                self.view.endEditing(true)
            }
            return true
            }        else{
            return true
        }
    }
    @objc func textFieldDidChange(textField: UITextField){
        let string = textField.text
        if string!.count <= 1{
            if string!.count == 1{
                if textField.tag == 1{
                    tf2.becomeFirstResponder()
                }else if textField.tag == 2{
                    tf3.becomeFirstResponder()

                }else if textField.tag == 3{
                    tf4.becomeFirstResponder()

                }else{
                    self.view.endEditing(true)
                }
                
                }else if string!.count == 0{
                    if textField.tag == 1{
//                        tf2.becomeFirstResponder()
                    }else if textField.tag == 2{
                        tf1.becomeFirstResponder()

                    }else if textField.tag == 3{
                        tf2.becomeFirstResponder()

                    }else{
                        tf3.becomeFirstResponder()
                    }
                    
                    }
            }
        
    }
    
    
    @IBAction func resendCode(_ sender: Any) {
        let urlString = (sendOtp as NSString).appendingPathComponent("\(selectedDic["callingCodes"] as! String)\(phoneNumber!)")
    ResponseFromApi.shared.getRequestForParameters(url: urlString) { (ab, resptype) in
        print(ab,resptype)
        if resptype == true{
            if let dict = (ab as? NSDictionary){
                if let stat = dict["Status"] as? String{
                    if stat ==  "Success"{
                        DispatchQueue.main.async {
                            Alert.showBasic(title: "OTP", message: "Otp successfully resent", vc: self)
                        }
                       
                    }
                } else  if let msg = dict["statusMessage"] as? String{
                    Alert.showBasic(title: "Error", message: msg, vc: self)
                }

            }
        }
    }
    }
    @IBAction func confirm(_ sender: Any) {
        for i in [tf1.text!,tf2.text!,tf3.text!,tf4.text!]{
            if i.isEmpty{
                print("please enter otp")
                return
            }
        }
        let otp = tf1.text!+tf2.text!+tf3.text!+tf4.text!
        let urlString = (verifyOTP as NSString).appendingPathComponent("\(dialCode!)\(phoneNumber!)/code/\(otp)")
        ResponseFromApi.shared.getRequestForParameters(url: urlString) { (ab, resptype) in
            print(ab,resptype)
            
                if let dict = (ab as? NSDictionary){
                    if let stat = dict["statusCode"] as? Int{
                        if stat ==  200{
                            DispatchQueue.main.async {
                                let viewCOntroller = self.storyboard?.instantiateViewController(withIdentifier: "EnterSignUpDetailsViewController") as? EnterSignUpDetailsViewController
                                viewCOntroller?.phoneNumber = self.phoneNumber
                                viewCOntroller?.selectedDic = self.selectedDic
                                self.navigationController?.pushViewController(viewCOntroller!, animated: true)
                            }
                           
                        }else{
                            if let msg = dict["responseMessage"] as? String{
                                Alert.showBasic(title: "Error", message: msg, vc: self)
                            }
                        }
                        
                    }
                }
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
