//
//  ChangeForgotPasswordViewController.swift
//  SOSUDA PAY
//
//  Created by Saifur Rahman on 22/03/21.
//

import UIKit
import DTTextField
class ChangeForgotPasswordViewController: UIViewController {
    @IBOutlet weak var confirmPassword: DTTextField!
    @IBOutlet weak var password: DTTextField!
    
    @IBOutlet weak var topNavBar: TopHeader!
    @IBOutlet weak var topDescription: UILabel!
    @IBOutlet weak var otpTextfield: DTTextField!
    var phoneNumber:String?
    var dialCode:String?
    override func viewDidLoad() {
        super.viewDidLoad()
        setupToHideKeyboardOnTapOnView()
        topDescription.text = topDescription.text! + phoneNumber!
        topNavBar.title.text = "Reset Password"
        topNavBar.backButtonWidth.constant = 0
        settextField(txtField: otpTextfield, errorMessage: "Invalid OTP", placeholder: "Enter OTP", leftImage: UIImage(systemName: "iphone.homebutton")!)
        settextField(txtField: password, errorMessage: "Password must contain atleast 8 characters including Upper,Lowercase,Special character and Number", placeholder: "Password", leftImage: UIImage(systemName: "lock")!)
        settextField(txtField: confirmPassword, errorMessage: "Confirm password not matched", placeholder: "Confirm Password", leftImage: UIImage(systemName: "lock")!)
        
    }
    // MARK: - SettingCustum_textfield_settings
    func settextField(txtField:DTTextField,errorMessage:String,placeholder:String,leftImage:UIImage){
        txtField.borderColor = #colorLiteral(red: 0.1957393885, green: 0.2087394297, blue: 0.7472228408, alpha: 1)
        txtField.borderWidth = 2
        txtField.borderStyle = .roundedRect
        txtField.errorMessage = errorMessage
        txtField.placeholder = placeholder
        txtField.placeholderColor = #colorLiteral(red: 0.2842571437, green: 0.7277087569, blue: 0.9657751918, alpha: 1)
        txtField.floatPlaceholderColor = #colorLiteral(red: 0.2842571437, green: 0.7277087569, blue: 0.9657751918, alpha: 1)
        txtField.floatPlaceholderActiveColor = #colorLiteral(red: 0.1957393885, green: 0.2087394297, blue: 0.7472228408, alpha: 1)
        txtField.animateFloatPlaceholder = true
        txtField.paddingYFloatLabel = 10
        txtField.paddingXFloatLabel = 25
        txtField.floatPlaceholderFont = UIFont.systemFont(ofSize: 12)
        //txtField.addPadding(UITextField.PaddingSide.left(20))
        let leftImgView = UIView.init(frame: CGRect(x: 10, y: 10, width: 50, height: 50))
        let imag = UIImageView.init(frame: CGRect(x: 15, y: 25, width: 20, height: 20))
        imag.image = leftImage
        leftImgView.addSubview(imag)
        txtField.leftView = leftImgView
        txtField.leftViewMode = .always
        
    }
    
    @IBAction func resetPassword(_ sender: Any) {
        if validate(){
            ResponseFromApi.shared.postRequestForParameters(url: changeforgettenPassword, params: ["mobileNumber": dialCode!+phoneNumber!,"password": password.text?.trimmingCharacters(in: .whitespaces), "otpCode": otpTextfield.text?.trimmingCharacters(in: .whitespaces)]) { (resp,dat, isSuccess) in
                print(resp,isSuccess)
                if isSuccess{
                    if let stat = (resp as! NSDictionary)["status"] as? Int{
                        if stat ==  1{
                        UserDefaults.standard.set(dat ,forKey:"data")
                        UserDefaults.standard.set((resp as! NSDictionary) ,forKey:"UserDictionary")
                            DispatchQueue.main.async {
                                let viewCOntroller = self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as? LoginViewController
                           
                                self.navigationController?.pushViewController(viewCOntroller!, animated: true)
                                
                            }
                        
                    }else{
                        Alert.showBasic(title: "Password", message: (resp as! NSDictionary)["message"] as! String, vc: self)
                    }
                    }
                }
                
            }
            
        }
    }
    func validate()-> Bool{
        var val = true
        if otpTextfield.text!.isEmpty{
            otpTextfield.showError()
            val = false
        }
        if !password.text!.isValidPassword(){
            password.showError()
            val = false
        }
        if confirmPassword.text!.isEmpty{
            confirmPassword.showError()
            val = false
        }
        if confirmPassword.text! != password.text!{
            confirmPassword.showError()
            val = false
        }
        return val
    }
}
