//
//  ViewController.swift
//  SOSUDA PAY
//
//  Created by Saifur Rahman on 03/03/21.
//

import UIKit
// ADCountryPicker
class LoginViewController: UIViewController,UITextFieldDelegate {
    
    @IBOutlet weak var countryTableView: UITableView!
    @IBOutlet weak var countryCodeLbl: UILabel!
    @IBOutlet weak var countryNameLbl: UILabel!
    @IBOutlet weak var countryImageView: UIImageView!
    @IBOutlet weak var phoneTextfield: Designabletextfield!
    @IBOutlet weak var passwordTextfield: Designabletextfield!
    var arryOfCountry  = NSArray()
    var selectedDic = NSDictionary()
    //  let picker =  ADCountryPicker()
    var pickerNavigationController : UINavigationController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupToHideKeyboardOnTapOnView()
        self.countryTableView.isHidden = true
        // Do any additional setup after loading the view.
        countryTableView.tableViewSetupWithViewC(ViewController: self, cellArrayToRegister: [CountryTableViewCell.getCellIdentifier()])
        //   pickerNavigationController =  UINavigationController(rootViewController: picker)
        //  picker.delegate = self
        phoneTextfield.delegate = self
        ResponseFromApi.shared.getRequestForParameters(url: getCountryList) { (resp, isSuccess) in
            print(resp,isSuccess)
            if isSuccess{
                if let _ = (resp as! NSDictionary)["data"] as? NSArray{
                    self.arryOfCountry = ((resp as! NSDictionary)["data"] as! NSArray)
                    self.countryTableView.delegate = self
                    self.countryTableView.dataSource = self
                    self.countryTableView.reloadData()
                }
            }
        }
    }
    @IBAction func selectCountry(_ sender: Any) {
        // let picker = ADCountryPicker()
        if let _ = selectedDic["alpha3Code"]{
        self.countryTableView.isHidden = false
        }else{
            if Reachability.isConnectedToNetwork(){
                ResponseFromApi.shared.getRequestForParameters(url: getCountryList) { (resp, isSuccess) in
                    print(resp,isSuccess)
                    if isSuccess{
                        if let _ = (resp as! NSDictionary)["data"] as? NSArray{
                            self.arryOfCountry = ((resp as! NSDictionary)["data"] as! NSArray)
                            self.countryTableView.delegate = self
                            self.countryTableView.dataSource = self
                            self.countryTableView.reloadData()
                            self.countryTableView.isHidden = false

                        }
                    }
                }
            }else{
                Alert.showBasic(title: "Error", message: "Please connect to an active internet connection", vc: self)
            }
        }
        //  self.present(pickerNavigationController!, animated: true, completion: nil)
        //self.navigationController?.pushViewController(picker, animated: true)
    }
    @IBAction func signUp(_ sender: Any) {
        let viewCOntroller = storyboard?.instantiateViewController(withIdentifier: "SignupViewController") as? SignupViewController
        self.navigationController?.pushViewController(viewCOntroller!, animated: true)
    }
    
    @IBAction func loginNow(_ sender: Any) {
        if phoneTextfield.text!.count < 10{
            Alert.showBasic(title: "Phone Number", message: "Please enter a valid phone Number", vc: self)
        }else if !(passwordTextfield.text?.isValidPassword())!{
            Alert.showBasic(title: "Password", message: "pasword must contain Uppercase characters, Lowercase characters, Digits,Special characters", vc: self)
        }else if passwordTextfield.text!.count < 8{
            Alert.showBasic(title: "Password", message: "password must be atleast 8 characters long", vc: self)
        }
        
        let deviceId = UIDevice.current.identifierForVendor!.uuidString
        if let _ = selectedDic["alpha3Code"]{
        ResponseFromApi.shared.postRequestForParameters(url: loginAPI, params: ["callingCode":countryCodeLbl.text!,"countryAlphaCode":selectedDic["alpha3Code"] as! String,"deviceId":deviceId,"deviceType":"iOS","fcmToken":"egrfhg","mobile":phoneTextfield.text!,"password":passwordTextfield.text?.trimmingCharacters(in: .whitespaces),"userId": "\(countryCodeLbl.text!+(phoneTextfield.text?.trimmingCharacters(in: .whitespaces))!)"]) { (resp,dat, isSuccess) in
            print(resp,isSuccess)
            if isSuccess{
                if let suc_ = (resp as! NSDictionary)["token"] as? String{
                    UserDefaults.standard.set(dat ,forKey:"data")
                    UserDefaults.standard.set((resp as! NSDictionary) ,forKey:"UserDictionary")
                        DispatchQueue.main.async {
                            let viewCOntroller = self.storyboard?.instantiateViewController(withIdentifier: "MainTabBarViewController") as? MainTabBarViewController
                       
                            self.navigationController?.pushViewController(viewCOntroller!, animated: true)
                        }
                    
                }else{
                    Alert.showBasic(title: "Password", message: "Wrong Password", vc: self)

                }
            }
            
        }
        }else{
            Alert.showBasic(title: "Connectivity", message: "Can't fatch country details", vc: self)
        }
    }
    @IBAction func forgotPassword(_ sender: Any) {
        let viewCOntroller = self.storyboard?.instantiateViewController(withIdentifier: "SignupViewController") as? SignupViewController
        viewCOntroller?.type = "forgot password"
        self.navigationController?.pushViewController(viewCOntroller!, animated: true)
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let (shouldReturn,_) = textField.maxLenghtIntextfield(length: 16, range: range, string: string)
        return shouldReturn
    }
}


//extension LoginViewController: ADCountryPickerDelegate{
//    func countryPicker(_ picker: ADCountryPicker, didSelectCountryWithName name: String, code: String, dialCode: String) {
//        self.countryImageView.image =  picker.getFlag(countryCode: code)
//        self.countryCodeLbl.text = dialCode
//        self.countryNameLbl.text = name
//        picker.dismiss(animated: true, completion: nil)
//    }


//}
extension LoginViewController:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arryOfCountry.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CountryTableViewCell") as! CountryTableViewCell
        cell.countrynameLabel.text = (self.arryOfCountry[indexPath.row] as! NSDictionary)["name"] as!String
        cell.countryCodeLabel.text = (self.arryOfCountry[indexPath.row] as! NSDictionary)["callingCodes"]
            as!String
        if let _ = selectedDic["name"]{
            
        }else{
            if (self.arryOfCountry[indexPath.row] as! NSDictionary)["name"] as!String == "India"{
            selectedDic = (self.arryOfCountry[indexPath.row] as! NSDictionary)
            }
        }
            //(self.arryOfCountry[indexPath.row] as! NSDictionary)["name"] as!String
        return cell
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        countryNameLbl.text = (self.arryOfCountry[indexPath.row] as! NSDictionary)["name"] as!String
        countryCodeLbl.text = (self.arryOfCountry[indexPath.row] as! NSDictionary)["callingCodes"] as! String
        selectedDic = self.arryOfCountry[indexPath.row] as! NSDictionary
        countryTableView.isHidden = true
        let urlIs = (getFlagImage as NSString).appendingPathComponent(selectedDic["flag"] as! String)
        
        if let url = URL(string: urlIs) {
            let task = URLSession.shared.dataTask(with: url) { data, response, error in
                guard let data = data, error == nil else { return }
                
                DispatchQueue.main.async { /// execute on main thread
                    self.countryImageView.image = UIImage(data: data)
                }
            }
            
            task.resume()
        }
    }
}
