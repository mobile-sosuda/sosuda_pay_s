//
//  SignupViewController.swift
//  SOSUDA PAY
//
//  Created by Saifur Rahman on 04/03/21.
//

import UIKit
//import ADCountryPicker

class SignupViewController: UIViewController {
    @IBOutlet weak var countryCodeLbl: UILabel!
    @IBOutlet weak var alreadyHaveAccBtn: UIButton!
    @IBOutlet weak var countryTableView: UITableView!
    @IBOutlet weak var countryNameLbl: UILabel!
    @IBOutlet weak var mobileTextField: Designabletextfield!
    @IBOutlet weak var countryImageView: UIImageView!
    var selectedDic = NSDictionary()
    var type :String? = "Signup"
    var pickerNavigationController : UINavigationController?
    var arryOfCountry = NSArray()
    @IBOutlet weak var headerTopView: TopHeader!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupToHideKeyboardOnTapOnView()
        countryTableView.isHidden = true
        headerTopView.backButtonWidth.constant = 0.0
        if type == "Signup"{
            alreadyHaveAccBtn.isHidden = false
        headerTopView.title.text  = "User Registration"
        }else{
         //   alreadyHaveAccBtn.setTitle("", for: <#T##UIControl.State#>)
            alreadyHaveAccBtn.isHidden = true
            headerTopView.title.text  = "Forgot Password"
        }
        headerTopView.backButton.setImage(UIImage(systemName: "chevron.backward"), for: .normal)
        countryTableView.tableViewSetupWithViewC(ViewController: self, cellArrayToRegister: [CountryTableViewCell.getCellIdentifier()])
//        pickerNavigationController =  UINavigationController(rootViewController: picker)
//        picker.delegate = self
        mobileTextField.delegate = self
        ResponseFromApi.shared.getRequestForParameters(url: getCountryList) { (resp, isSuccess) in
            print(resp,isSuccess)
            if isSuccess{
                if let _ = (resp as! NSDictionary)["data"] as? NSArray{
                    self.arryOfCountry = ((resp as! NSDictionary)["data"] as! NSArray)
                    self.countryTableView.delegate = self
                    self.countryTableView.dataSource = self
                    self.countryTableView.reloadData()
                }
            }
        }
        // Do any additional setup after loading the view.
    }
    
    
    @IBAction func selectCountry(_ sender: Any) {
        countryTableView.isHidden = false
      //  self.present(pickerNavigationController!, animated: true, completion: nil)
    }
    @IBAction func signUp(_ sender: Any) {
        if validate(){
            var urlString = (sendOtp as NSString).appendingPathComponent("\(selectedDic["callingCodes"] as! String)\(mobileTextField.text!)")
            if type != "Signup"{
                urlString = (forgetPassword as NSString).appendingPathComponent("\(selectedDic["callingCodes"] as! String)\(mobileTextField.text!)")
            }
        ResponseFromApi.shared.getRequestForParameters(url: urlString) { (ab, resptype) in
            print(ab,resptype)
            if resptype == true{
                if let dict = (ab as? NSDictionary){
                    if let stat = dict["Status"] as? String{
                        if stat ==  "Success"{
                            DispatchQueue.main.async {
                                if self.type == "Signup" {
                                let viewCOntroller = self.storyboard?.instantiateViewController(withIdentifier: "OTPViewController") as? OTPViewController
                                viewCOntroller?.phoneNumber = self.mobileTextField.text!
                                viewCOntroller?.dialCode = (self.selectedDic["callingCodes"] as! String)
                                viewCOntroller?.selectedDic = self.selectedDic
                                self.navigationController?.pushViewController(viewCOntroller!, animated: true)
                                }else{
                                    let viewCOntroller = self.storyboard?.instantiateViewController(withIdentifier: "ChangeForgotPasswordViewController") as? ChangeForgotPasswordViewController
                                    viewCOntroller?.phoneNumber = self.mobileTextField.text!
                                    viewCOntroller?.dialCode = (self.selectedDic["callingCodes"] as! String)
                                  //  viewCOntroller?.selectedDic = self.selectedDic
                                    self.navigationController?.pushViewController(viewCOntroller!, animated: true)
                                }
                            }
                        }
                    } else  if let msg = dict["statusMessage"] as? String{
                        Alert.showBasic(title: "Error", message: msg, vc: self)
                    }

                }
            }
        }
        }
    }
    @IBAction func goBackToSignIn(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    func validate()-> Bool{
        if (mobileTextField.text?.trimmingCharacters(in: .whitespaces).count)! < 10{
           
            Alert.showBasic(title: "Invalid credential", message: "Please enter a valid phone number", vc: self)
            return false
        }else{
            return true
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension SignupViewController:UITextFieldDelegate{
//    func countryPicker(_ picker: ADCountryPicker, didSelectCountryWithName name: String, code: String, dialCode: String) {
//        countryNameLbl.text = name
//        countryCodeLbl.text = dialCode
//        countryImageView.image = picker.getFlag(countryCode: code)
//    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let (shouldReturn,count) = textField.maxLenghtIntextfield(length: 18, range: range, string: string)
        return shouldReturn
    }
    
}
extension SignupViewController:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arryOfCountry.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CountryTableViewCell") as! CountryTableViewCell
        cell.countrynameLabel.text = (self.arryOfCountry[indexPath.row] as! NSDictionary)["name"] as!String
        cell.countryCodeLabel.text = (self.arryOfCountry[indexPath.row] as! NSDictionary)["callingCodes"]
            as!String
        if let _ = selectedDic["name"]{
            
        }else{
            if (self.arryOfCountry[indexPath.row] as! NSDictionary)["name"] as!String == "India"{
            selectedDic = (self.arryOfCountry[indexPath.row] as! NSDictionary)
            }
        }
            //(self.arryOfCountry[indexPath.row] as! NSDictionary)["name"] as!String
        return cell
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        countryNameLbl.text = (self.arryOfCountry[indexPath.row] as! NSDictionary)["name"] as!String
        countryCodeLbl.text = (self.arryOfCountry[indexPath.row] as! NSDictionary)["callingCodes"] as! String
        selectedDic = self.arryOfCountry[indexPath.row] as! NSDictionary
        countryTableView.isHidden = true
        let urlIs = (getFlagImage as NSString).appendingPathComponent(selectedDic["flag"] as! String)
        
        if let url = URL(string: urlIs) {
            let task = URLSession.shared.dataTask(with: url) { data, response, error in
                guard let data = data, error == nil else { return }
                
                DispatchQueue.main.async { /// execute on main thread
                    self.countryImageView.image = UIImage(data: data)
                }
            }
            
            task.resume()
        }
    }
}
