//
//  ChangePasswordViewController.swift
//  SOSUDA PAY
//
//  Created by Saifur Rahman on 19/04/21.
//

import UIKit

class ChangePasswordViewController: UIViewController {
    @IBOutlet weak var topNavBar: TopHeader!
    @IBOutlet weak var oldPassField: Designabletextfield!
    
    @IBOutlet weak var confirmPassField: Designabletextfield!
    @IBOutlet weak var newPassField: Designabletextfield!
    override func viewDidLoad() {
        super.viewDidLoad()
        topNavBar.title.text = "Change Password"
        topNavBar.title.textColor = .white
        topNavBar.backButton.setImage(UIImage(systemName: "chevron.backward"), for: .normal)
        topNavBar.backButton.tintColor = .white
        topNavBar.backButtonWidth.constant = 60
        // Do any additional setup after loading the view.
    }
    // MARK: - Change Password Action
    @IBAction func changePasswordAction(_ sender: Any) {
    }


}
