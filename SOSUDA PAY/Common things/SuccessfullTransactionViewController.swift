//
//  SuccessfullTransactionViewController.swift
//  SOSUDA PAY
//
//  Created by Saifur Rahman on 06/04/21.
//

import UIKit

class SuccessfullTransactionViewController: UIViewController {

    @IBOutlet weak var topNavBar: TopHeader!
    @IBOutlet weak var amountField: UILabel!
    var amount = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        topNavBar.title.text = "Transaction"
        topNavBar.title.textColor = .white
        topNavBar.backButton.setImage(UIImage(systemName: "chevron.backward"), for: .normal)
        topNavBar.backButton.tintColor = .white
        topNavBar.backButtonWidth.constant = 60
        amountField.text = "₹ "+amount
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
