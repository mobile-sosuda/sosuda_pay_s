//
//  SingleButtonTableViewCell.swift
//  SOSUDA PAY
//
//  Created by Saifur Rahman on 04/04/21.
//

import UIKit

class SingleButtonTableViewCell: UITableViewCell {
    @IBOutlet weak var centerButton: DesignableButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
