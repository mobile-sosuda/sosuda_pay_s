//
//  SavedCards.swift
//  SOSUDA PAY
//
//  Created by Saifur Rahman on 29/03/21.
//

// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let welcome = try? newJSONDecoder().decode(Welcome.self, from: jsonData)

// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let welcome = try? newJSONDecoder().decode(Welcome.self, from: jsonData)

import Foundation

// MARK: - Welcome
struct SavedCards: Codable {
    let object: String
    let data: [DataForcard]
    let hasMore: Bool
    let url: String

    enum CodingKeys: String, CodingKey {
        case object, data
        case hasMore = "has_more"
        case url
    }
}

// MARK: - Datum
struct DataForcard: Codable {
    let id, object: String
    let account, addressCity, addressCountry, addressLine1: JSONNull?
    let addressLine1Check, addressLine2, addressState, addressZip: JSONNull?
    let addressZipCheck, availablePayoutMethods: JSONNull?
    let brand, country: String
    let currency: JSONNull?
    let customer, cvcCheck: String
    let defaultForCurrency, datumDescription, dynamicLast4: JSONNull?
    let expMonth, expYear: Int
    let fingerprint, funding: String
    let iin, issuer: JSONNull?
    let last4: String
    let metadata: Metadata
    let name, tokenizationMethod: JSONNull?

    enum CodingKeys: String, CodingKey {
        case id, object, account
        case addressCity = "address_city"
        case addressCountry = "address_country"
        case addressLine1 = "address_line1"
        case addressLine1Check = "address_line1_check"
        case addressLine2 = "address_line2"
        case addressState = "address_state"
        case addressZip = "address_zip"
        case addressZipCheck = "address_zip_check"
        case availablePayoutMethods = "available_payout_methods"
        case brand, country, currency, customer
        case cvcCheck = "cvc_check"
        case defaultForCurrency = "default_for_currency"
        case datumDescription = "description"
        case dynamicLast4 = "dynamic_last4"
        case expMonth = "exp_month"
        case expYear = "exp_year"
        case fingerprint, funding, iin, issuer, last4, metadata, name
        case tokenizationMethod = "tokenization_method"
    }
}

// MARK: - Metadata
struct Metadata: Codable {
}
