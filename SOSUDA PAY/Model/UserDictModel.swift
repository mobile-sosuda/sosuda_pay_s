//
//  UserDictModel.swift
//  SOSUDA PAY
//
//  Created by Saifur Rahman on 20/03/21.
//

import Foundation
// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let welcome = try? newJSONDecoder().decode(Welcome.self, from: jsonData)

import Foundation

// MARK: - Welcome
struct UserDictModel: Codable {
    let userID, mobileNumber, countryAlphaCode, callingCode: String
    let firstName, lastName, token: String
    let sessionID: Int
    let stripeCustomerID: String
    let subscriptionID: Int
    let subscription: Subscription
    let userscription: Userscription
    let userLimit: UserLimit

    enum CodingKeys: String, CodingKey {
        case userID = "userId"
        case mobileNumber, countryAlphaCode, callingCode, firstName, lastName, token, sessionID
        case stripeCustomerID = "stripeCustomerId"
        case subscriptionID = "subscriptionId"
        case subscription, userscription, userLimit
    }
}

// MARK: - Subscription
struct Subscription: Codable {
    let subscriptionID: Int
    let name: String
    let maxLimitOfDay, maxHitOfDay, domesticCharge, internationalCharge: Int
    let serviceCharge, validLimitDays: Int
    let isActive: Bool

    enum CodingKeys: String, CodingKey {
        case subscriptionID = "subscriptionId"
        case name, maxLimitOfDay, maxHitOfDay, domesticCharge, internationalCharge, serviceCharge, validLimitDays, isActive
    }
}

// MARK: - UserLimit
struct UserLimit: Codable {
    let usrePerDayLimitID: Int
    let userID: String
    let subscriptionID, totalAmountLimit, usedAmount, tolalNumberOfHit: Int
    let usedNumberOfHit: Int

    enum CodingKeys: String, CodingKey {
        case usrePerDayLimitID = "usrePerDayLimitId"
        case userID = "userId"
        case subscriptionID = "subscriptionId"
        case totalAmountLimit, usedAmount, tolalNumberOfHit, usedNumberOfHit
    }
}

// MARK: - Userscription
struct Userscription: Codable {
    let userSubscriptionID: Int
    let userID: String
    let subscriptionID: Int
    let createdDate: String

    enum CodingKeys: String, CodingKey {
        case userSubscriptionID = "userSubscriptionId"
        case userID = "userId"
        case subscriptionID = "subscriptionId"
        case createdDate
    }
}
