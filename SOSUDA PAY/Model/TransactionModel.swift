//
//  TransactionModel.swift
//  SOSUDA PAY
//
//  Created by Saifur Rahman on 01/04/21.
//

import Foundation
// MARK: - Welcome
struct AllTransactions: Codable {
    let status: Bool
    let statusCode: Int
    let message: String
    let data: [Transaction]
}

// MARK: - Datum
struct Transaction: Codable {
    let userID, txnID, txnDate: String
    let isP2PTransaction: Bool
    let crDR: String
    let txnAmount, balance: Double
    let txnSourceName: String

    enum CodingKeys: String, CodingKey {
        case userID = "userId"
        case txnID = "txnId"
        case txnDate, isP2PTransaction
        case crDR = "crDr"
        case txnAmount, balance, txnSourceName
    }
}
