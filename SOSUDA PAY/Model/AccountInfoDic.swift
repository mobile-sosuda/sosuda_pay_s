// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let welcome = try? newJSONDecoder().decode(Welcome.self, from: jsonData)

import Foundation

// MARK: - Welcome
struct AccountInfo: Codable {
    let status: Bool
    let statusCode: Int
    let message: String
    let data: [Datum]
}
// MARK: - Datum
struct Datum: Codable {
    let userID, txnID, txnDate: String
    let isP2Ptransaction: Bool
    let crDR: String
    let txnAmount: Double
    let balance: Int
    let message, walletType, industryTypeID: String?
    let merchantRefID, paymentMode, authMode, paymentTypeID: JSONNull?
    let bankCode, callBackURL, createdOn: JSONNull?
    let updatedOn: String

    enum CodingKeys: String, CodingKey {
        case userID = "userId"
        case txnID = "txnId"
        case txnDate
        case isP2Ptransaction = "isP2ptransaction"
        case crDR = "crDr"
        case txnAmount, balance, message, walletType
        case industryTypeID = "industryTypeId"
        case merchantRefID = "merchantRefId"
        case paymentMode, authMode
        case paymentTypeID = "paymentTypeId"
        case bankCode
        case callBackURL = "callBackUrl"
        case createdOn, updatedOn
    }
}

// MARK: - Encode/decode helpers

class JSONNull: Codable, Hashable {

    public static func == (lhs: JSONNull, rhs: JSONNull) -> Bool {
        return true
    }

    public var hashValue: Int {
        return 0
    }

    public init() {}

    public required init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()
        if !container.decodeNil() {
            throw DecodingError.typeMismatch(JSONNull.self, DecodingError.Context(codingPath: decoder.codingPath, debugDescription: "Wrong type for JSONNull"))
        }
    }

    public func encode(to encoder: Encoder) throws {
        var container = encoder.singleValueContainer()
        try container.encodeNil()
    }
}
