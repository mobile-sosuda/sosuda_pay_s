//
//  TransactionConversionModel.swift
//  SOSUDA PAY
//
//  Created by Saifur Rahman on 14/04/21.
//

// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let welcome = try? newJSONDecoder().decode(Welcome.self, from: jsonData)

import Foundation

// MARK: - Welcome
struct TransactionConversionModel: Codable {
    let status: Bool
    let statusCode: Int
    let message: String
    let data: TransactionConversion
}

// MARK: - DataClass
struct TransactionConversion: Codable {
    let senderUserID: String
    let senderSubscriptionID: Int
    let senderCountryAlphaCode: String
    let sendingAmount: Int
    let reciverCountryAlphaCode: String
    let transactionCharge: Double
    let serviceCharge, excengeRate: Int
    let recivingAmountbeforExchange, finalAmountAtReciverSide: Double
    let subscriptionDetails: SubscriptionDetails
    let perDayLimit: PerDayLimit
    let senderCountry, reciverCountry: ErCountry
    let transationDetailsID: Int

    enum CodingKeys: String, CodingKey {
        case senderUserID = "senderUserId"
        case senderSubscriptionID = "senderSubscriptionId"
        case senderCountryAlphaCode, sendingAmount, reciverCountryAlphaCode, transactionCharge, serviceCharge, excengeRate, recivingAmountbeforExchange, finalAmountAtReciverSide, subscriptionDetails, perDayLimit, senderCountry, reciverCountry
        case transationDetailsID = "transationDetailsId"
    }
}

// MARK: - PerDayLimit
struct PerDayLimit: Codable {
    let usrePerDayLimitID: Int
    let userID: String
    let subscriptionID, totalAmountLimit, usedAmount, tolalNumberOfHit: Int
    let usedNumberOfHit: Int

    enum CodingKeys: String, CodingKey {
        case usrePerDayLimitID = "usrePerDayLimitId"
        case userID = "userId"
        case subscriptionID = "subscriptionId"
        case totalAmountLimit, usedAmount, tolalNumberOfHit, usedNumberOfHit
    }
}

// MARK: - ErCountry
struct ErCountry: Codable {
    let countryMasterID: Int
    let name, alpha3Code, callingCodes, symbol: String
    let code, curName, flag: String

    enum CodingKeys: String, CodingKey {
        case countryMasterID = "countryMasterId"
        case name, alpha3Code, callingCodes, symbol, code
        case curName = "cur_name"
        case flag
    }
}

// MARK: - SubscriptionDetails
struct SubscriptionDetails: Codable {
    let subscriptionID: Int
    let name: String
    let maxLimitOfDay, maxHitOfDay, domesticCharge, internationalCharge: Int
    let serviceCharge, validLimitDays: Int
    let isActive: Bool

    enum CodingKeys: String, CodingKey {
        case subscriptionID = "subscriptionId"
        case name, maxLimitOfDay, maxHitOfDay, domesticCharge, internationalCharge, serviceCharge, validLimitDays, isActive
    }
}

