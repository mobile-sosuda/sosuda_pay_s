//
//  DashboardHomeViewController.swift
//  SOSUDA PAY
//
//  Created by Saifur Rahman on 11/03/21.
//

import UIKit

class DashboardHomeViewController: UIViewController {
    @IBOutlet weak var recentTransactionViewHeight: NSLayoutConstraint!
    @IBOutlet weak var hideBalanceViewHeight: NSLayoutConstraint!
    @IBOutlet weak var recentTransactionsTable: UITableView!
    @IBOutlet weak var botttomView: UIView!
    @IBOutlet weak var noTransactionErrorImage: UIImageView!
    @IBOutlet weak var pagerView: UIPageControl!
    @IBOutlet weak var notransactionLabel: UILabel!
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var tapToRevealBal: UIButton!
    @IBOutlet weak var addMoneyView: UIView!
    @IBOutlet weak var sendMoneyView: UIView!
    @IBOutlet weak var cashDepositView: UIView!
    @IBOutlet weak var cashWithdrawalView: UIView!
    var godown = CGFloat(0.0)
    var dictAccount = [Transaction]()
    var bannerarr = [UIImage(named: "ad_1.png"),UIImage(named: "ad_2.png"),UIImage(named: "ad_3.png")]
    @IBOutlet weak var bannerCollectionview: UICollectionView!
    @IBOutlet weak var recenttransactionViewbottomAnchorConstant: NSLayoutConstraint!
    @IBOutlet weak var topNavBar: TopHeader!
    var accountBalance = 7.5
    var recentTransactionsArray = [String]()
    var recentTransactionsArrayName = ["Shams Tabrez", "Kal Al","Diana","Hermoini","Altamash","SuperMan","Hermoini","Altamash","SuperMan"]
    
    
    @IBAction func hideBalance(_ sender: Any) {
        tapToRevealBal.setTitle("Tap To Reveal", for: .normal)
        UIView.animate(withDuration: 0.5) {
            self.topView.frame.size.height = self.topView.frame.height - 50
            self.hideBalanceViewHeight.constant = 0
            
        }
    }
 
    override func viewDidLoad() {
        super.viewDidLoad()
        topNavBar.backButton.setImage(UIImage(systemName: "person.circle.fill"), for: .normal)
        topNavBar.backButtonWidth.constant = 60
        topNavBar.backButton.tintColor = .lightGray
        topNavBar.logoutButton.isHidden = false
        topNavBar.logoutButton.setImage(UIImage(systemName: "power"), for: .normal)
        topNavBar.backButton.tintColor = .white
        topNavBar.logoutButton.tintColor = .white
        topNavBar.backgroundColor = #colorLiteral(red: 0.1957393885, green: 0.2087394297, blue: 0.7472228408, alpha: 1)
        bannerCollectionview.collectionViewSetupWithViewC(ViewController: self, cellArrayToRegister: [BannerCollectionViewCell.getCellIdentifier()])
        let layout = UICollectionViewFlowLayout()
        // layout.estimatedItemSize = CGSize(width: bannerCollectionview.frame.width, height: bannerCollectionview.frame.height)
        layout.itemSize = CGSize(width: bannerCollectionview.frame.width, height: bannerCollectionview.frame.height)
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = 0
        
        layout.scrollDirection = .horizontal
        bannerCollectionview.isPagingEnabled = true
        bannerCollectionview.collectionViewLayout = layout
        
        bannerCollectionview.delegate = self
        bannerCollectionview.dataSource = self
        let up = UISwipeGestureRecognizer(target: self, action: #selector(respondToSwipeGesture))
        up.direction = .up
        self.view.addGestureRecognizer(up)
        
        let swipeDown = UISwipeGestureRecognizer(target: self, action: #selector(respondToSwipeGesture))
        swipeDown.direction = .down
        self.view.addGestureRecognizer(swipeDown)
        recentTransactionsTable.tableViewSetupWithViewC(ViewController: self, cellArrayToRegister: [TransactionTableViewCell.getCellIdentifier()])
        recentTransactionsTable.delegate = self
        recentTransactionsTable.dataSource = self
        setup()
    }
    override func viewWillAppear(_ animated: Bool) {
        if let dic = UserDefaults.standard.value(forKey:"UserDictionary") {
            let firstName = (dic as! NSDictionary)["firstName"] as! String
            let subs = ((dic as! NSDictionary)["subscription"] as! NSDictionary)["name"] as? String
            if subs == "Non Subscriber"{
                topNavBar.title.text = firstName + " (STAG) - " + subs!
            }else{
            }
            if  let dict = UserDefaults.standard.value(forKey: "data"){
            let dictMod = try? JSONDecoder().decode(UserDictModel.self, from: dict as! Data)
            if let _ = dictMod{
                getAccountBal(dict: dictMod!)
            }
        }
        }
    }
    func setup(){
        addMoneyView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(addMoneyGesture(_:))))
    }
    @objc func addMoneyGesture(_ sender: UITapGestureRecognizer){
        let vc = storyboard?.instantiateViewController(withIdentifier: "AddMoneyViewController") as! AddMoneyViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    func getAccountBal(dict:UserDictModel){
        let urlString = (ACCOUNTBAL as NSString).appendingPathComponent("?userId=\(dict.userID)")
        ResponseFromApi.shared.getRequestWithHeader(url: urlString ,header: ["Authorization" : "Bearer \(dict.token)","Content-Type": "application/json"]) { (Response,dat,isSuccess) in
            if isSuccess{
                let dictAccount = try? JSONDecoder().decode(AccountInfo.self, from: dat!)
                self.accountBalance = Double(((dictAccount!).data[0].balance))
            }
        }
        getTransactions(pageNumber: "1", pageSize: "10", dict: dict)
      //  TransactionHistory
    }
    func getTransactions(pageNumber:String,pageSize:String,dict:UserDictModel){
        let urlString = (TRANSACTION_HISTORY as NSString).appendingPathComponent("?userId=\(dict.userID)$\(pageNumber)$\(pageSize)")
        ResponseFromApi.shared.getRequestWithHeader(url: urlString ,header: ["Authorization" : "Bearer \(dict.token)","Content-Type": "application/json"]) { (Response,dat,isSuccess) in
            if isSuccess{
                print(Response)
                let dictAccount1 = try? JSONDecoder().decode(AllTransactions.self, from: dat as! Data)
                if dictAccount1?.data != nil{
                    self.dictAccount = dictAccount1!.data
                    self.recentTransactionsTable.reloadData()
                }
             print(dictAccount1)
            }
        }
    }
    @objc func respondToSwipeGesture(gesture: UIGestureRecognizer) {
        if let swipeGesture = gesture as? UISwipeGestureRecognizer {
            switch swipeGesture.direction {
            case .right:
                print("Swiped right")
            case .down:
                print("Swiped down")
                UIView.animate(withDuration: 0.5) {
                    self.botttomView.transform = CGAffineTransform(translationX: 0, y: 0)
                }
            case .left:
                print("Swiped left")
            case .up:
                UIView.animate(withDuration: 0.5) {
                    let yyy = self.recenttransactionViewbottomAnchorConstant.constant
                    self.botttomView.transform = CGAffineTransform(translationX: 0, y: yyy)
                }
                print("Swiped up")
            default:
                break
            }
        }
    }
    @IBAction func revealBal(_ sender: Any) {
        (sender as? UIButton)?.setTitle("\(accountBalance)", for: .normal)
        UIView.animate(withDuration: 0.5) {
            self.topView.frame.size.height = self.topView.frame.height + 50

            self.hideBalanceViewHeight.constant = 20
        }
        //        ResponseFromApi.shared.postRequestForParameters(url: accountBal, params: ["userId":dict.userID],header: ["Authorization" : "Bearer\(dict.token)"]) { (Response, isSuccess) in
        //            let info = Response as! Any
        //            let dictAccount = info as! AccountInfo
        //            self.accountBalance = (dictAccount.data[0].balance)
        //        }
    }
}
extension DashboardHomeViewController:UICollectionViewDelegate,UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return bannerarr.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "BannerCollectionViewCell", for: indexPath) as! BannerCollectionViewCell
        cell.bannerImage.image = bannerarr[indexPath.row]
        cell.bannerImage.layer.cornerRadius = 10
        return cell
    }
    
    
    
}
extension DashboardHomeViewController:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.dictAccount.isEmpty{
            recentTransactionViewHeight.constant = 300
            tableView.isHidden = true
            noTransactionErrorImage.isHidden = false
            notransactionLabel.isHidden = false
        }else{
        recentTransactionViewHeight.constant = CGFloat(47 + (70 * self.dictAccount.count))
            
        recenttransactionViewbottomAnchorConstant.constant = -CGFloat(70 * self.dictAccount.count)
            if recentTransactionViewHeight.constant > (self.view.frame.height - 20){
                recentTransactionViewHeight.constant = self.view.frame.height - 47
                recenttransactionViewbottomAnchorConstant.constant = -CGFloat(self.view.frame.height - 47 - 40)

            }
            noTransactionErrorImage.isHidden = true
            notransactionLabel.isHidden = true
            tableView.isHidden = false
        }
        return self.dictAccount.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TransactionTableViewCell") as! TransactionTableViewCell
        let startingletter = self.dictAccount[indexPath.row].txnSourceName.first!.lowercased()
        cell.profileImage.image = UIImage(systemName: "\(startingletter).circle.fill")
        let theName = self.dictAccount[indexPath.row].txnSourceName.split(separator: " ")
        cell.profilename.text = theName.first! + " " + theName.last!
        if self.dictAccount[indexPath.row].crDR == "CR"{
            cell.amount.text = "+\(self.dictAccount[indexPath.row].txnAmount)"
            cell.amount.textColor = .green
            cell.dateOfTransaction.text = ""
        }else{
            cell.amount.text = "-\(self.dictAccount[indexPath.row].txnAmount)"
            cell.amount.textColor = .red
        }
       // cell.dateOfTransaction.text = 
        return cell
    }
   
}
