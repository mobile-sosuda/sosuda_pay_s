//
//  TransactionTableViewCell.swift
//  SOSUDA PAY
//
//  Created by Saifur Rahman on 31/03/21.
//

import UIKit

class TransactionTableViewCell: UITableViewCell {

    @IBOutlet weak var timeOfTransaction: UILabel!
    @IBOutlet weak var amount: UILabel!
    @IBOutlet weak var profilename: UILabel!
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var dateOfTransaction: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
