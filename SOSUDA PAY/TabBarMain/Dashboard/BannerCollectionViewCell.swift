//
//  BannerCollectionViewCell.swift
//  SOSUDA PAY
//
//  Created by Saifur Rahman on 17/03/21.
//

import UIKit

class BannerCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var bannerImage: UIImageView!
    @IBOutlet weak var backGroundView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        backGroundView.layer.cornerRadius = 10
    }

}
