//
//  AddAmountTableViewCell.swift
//  SOSUDA PAY
//
//  Created by Saifur Rahman on 03/04/21.
//

import UIKit

class AddAmountTableViewCell: UITableViewCell,UITextFieldDelegate {
    var   delegate : AddMoneyViewController?
    //var delegate : moneyEntered! = nil

    @IBOutlet weak var amountTextfield: Designabletextfield!
    override func awakeFromNib() {
        super.awakeFromNib()
        amountTextfield.delegate = self
        amountTextfield.becomeFirstResponder()

        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    @IBAction func predefinedAmount100(_ sender: Any) {
        amountTextfield.text = "100"
        amountTextfield.becomeFirstResponder()
    }
    @IBAction func predefinedAmount500(_ sender: Any) {
        amountTextfield.text = "500"
        amountTextfield.becomeFirstResponder()

    }
    @IBAction func predefinedAmount1000(_ sender: Any) {
        amountTextfield.text = "1000"
        amountTextfield.becomeFirstResponder()
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField.text!.isEmpty{
        delegate!.typedAmount(amountString: "0")
        }else{
            delegate!.typedAmount(amountString: textField.text!)
        }
    }
}
