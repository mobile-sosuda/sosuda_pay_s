//
//  AddMoneyViewController.swift
//  SOSUDA PAY
//
//  Created by Saifur Rahman on 03/04/21.
//

import UIKit
protocol moneyEntered {
    func typedAmount(amountString:String)
}
class AddMoneyViewController: UIViewController,moneyEntered {
    var amountEntered = "0"
    func typedAmount(amountString: String) {
        amountEntered = amountString
    }
    
    
    @IBOutlet weak var addMoneyTableview: UITableView!
    @IBOutlet weak var topNavBar: TopHeader!
    var arrayOfCards = [DataForcard]()
    var currency = "INR"
    override func viewDidLoad() {
        super.viewDidLoad()
        topNavBar.title.text = "Add Money"
        topNavBar.title.textColor = .white
        topNavBar.backButton.setImage(UIImage(systemName: "chevron.backward"), for: .normal)
        topNavBar.backButton.tintColor = .white
        topNavBar.backButtonWidth.constant = 60
        addMoneyTableview.delegate = self
        addMoneyTableview.dataSource = self
        addMoneyTableview.separatorStyle = .none
        addMoneyTableview.tableViewSetupWithViewC(ViewController: self, cellArrayToRegister: [AddAmountTableViewCell.getCellIdentifier(),SingleButtonTableViewCell.getCellIdentifier(),CardTableViewCell.getCellIdentifier()])
        getCards()
    }
    func getCards(){
            if  let dict = UserDefaults.standard.value(forKey: "data"){
            let dictMod = try? JSONDecoder().decode(UserDictModel.self, from: dict as! Data)
                let urlString = (STRIPEGETCARDS as NSString).appendingPathComponent("?customerId=\(dictMod!.stripeCustomerID)&limit=\(10)")
                ResponseFromApi.shared.getRequestWithHeader(url: urlString ,header: ["Authorization" : "Bearer \(dictMod!.token)","Content-Type": "application/json"]) { (Response,dat,isSuccess) in
                if isSuccess{
                    print(Response)
                    let dictAccount = try? JSONDecoder().decode(SavedCards.self, from: dat!)
                    self.arrayOfCards = dictAccount!.data
                    self.addMoneyTableview.reloadData()
                }
            }
            }
    }
}
extension AddMoneyViewController:UITableViewDelegate,UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        2
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 1
        }else{
        return arrayOfCards.count + 1
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0{
           let cell = tableView.dequeueReusableCell(withIdentifier: "AddAmountTableViewCell") as! AddAmountTableViewCell
            cell.delegate = self
            cell.selectionStyle = .none
            return cell
        }else if indexPath.row == arrayOfCards.count{
            let cell = tableView.dequeueReusableCell(withIdentifier: "SingleButtonTableViewCell") as! SingleButtonTableViewCell
            cell.centerButton.addTarget(self, action: #selector(addMoney), for: .touchUpInside)
            cell.selectionStyle = .none
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "CardTableViewCell") as! CardTableViewCell
            cell.selectionStyle = .none
            cell.deleteButtton.tag = indexPath.row
            cell.deleteButtton.addTarget(self, action: #selector(deleteCard(_:)), for: .touchUpInside)
            return cell
        }
    }
    @objc func addMoney(){
        self.view.endEditing(true)
        if Double(amountEntered)! < 1.0{
            Alert.showBasic(title: "Error", message: "Least mount you can charge is 1", vc: self)
return
        }
        if Reachability.isConnectedToNetwork(){
            if  let dict = UserDefaults.standard.value(forKey: "data"){
            let dictMod = try? JSONDecoder().decode(UserDictModel.self, from: dict as! Data)
            let urlString = (ADDMONEY as NSString)
                ResponseFromApi.shared.postRequestForParameters(url: urlString as String, params: ["amount": "\((Int(Double(amountEntered)!)))","customerId": dictMod!.stripeCustomerID,"currency": currency] ,header: ["Authorization" : "Bearer \(dictMod!.token)","Content-Type": "application/json"]) { (Response,dat,isSuccess) in
        if isSuccess{
            print(Response)
            if let stats = ((Response as! NSDictionary)["status"] as? String){
                if stats == "succeeded"{
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "SuccessfullTransactionViewController") as! SuccessfullTransactionViewController
                    vc.amount = "\(Int(Double(self.amountEntered)!))"
                    self.navigationController?.pushViewController(vc, animated: true)

                }else{
                    Alert.showBasic(title: "Failed", message: (Response as! NSDictionary)["message"] as! String, vc: self)

                }
            }else{
                Alert.showBasic(title: "Failed", message: (Response as! NSDictionary)["message"] as! String, vc: self)

            }
        }else{
            if let _ = Response as? NSDictionary{
            Alert.showBasic(title: "Failed", message: (Response as! NSDictionary)["message"] as! String, vc: self)
        }
        }
          
        } }
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row != arrayOfCards.count && indexPath.section != 0{
            let carddata = self.arrayOfCards[indexPath.row]
            
        }
    }
    @objc func deleteCard(_ sender: UIButton){
        
        if  let dict = UserDefaults.standard.value(forKey: "data"){
        let dictMod = try? JSONDecoder().decode(UserDictModel.self, from: dict as! Data)
            if Reachability.isConnectedToNetwork(){
                let carddata = self.arrayOfCards[sender.tag]
                let urlString = (DELETESTRIPECARD as NSString).appendingPathComponent("?CustomerId=\(dictMod!.stripeCustomerID)&CardId=\(carddata.id)")
            ResponseFromApi.shared.postRequestWithHeader(url: urlString ,header: ["Authorization" : "Bearer \(dictMod!.token)","Content-Type": "application/json"]) { (Response,dat,isSuccess) in
                
            if isSuccess{
                print(Response)
            }
                self.arrayOfCards.remove(at: sender.tag)
                self.addMoneyTableview.reloadSections(IndexSet(integer: 1), with: .fade)
            } }
        }
}
}

