//
//  LabelAndImageTableViewCell.swift
//  SOSUDA PAY
//
//  Created by Saifur Rahman on 16/04/21.
//

import UIKit

class LabelAndImageTableViewCell: UITableViewCell {
    @IBOutlet weak var bottomLabelheightVonstant: NSLayoutConstraint!
    
    @IBOutlet weak var switchButton: UISwitch!
    @IBOutlet weak var bottomLabel: UILabel!
    @IBOutlet weak var errorImageViewRight: UIImageView!
    @IBOutlet weak var profileDetailLabel: UILabel!
    @IBOutlet weak var lineSeperatorLeading: NSLayoutConstraint!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
