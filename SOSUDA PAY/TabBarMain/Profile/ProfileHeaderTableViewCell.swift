//
//  ProfileHeaderTableViewCell.swift
//  SOSUDA PAY
//
//  Created by Saifur Rahman on 16/04/21.
//

import UIKit

class ProfileHeaderTableViewCell: UITableViewCell {
    @IBOutlet weak var editBtn: UIButton!
    @IBOutlet weak var mobilenumberLabel: UILabel!
    @IBOutlet weak var NALabel: UILabel!
    @IBOutlet weak var editDetailsButton: UIButton!
    @IBOutlet weak var camWithProfileImage: UIImageView!
    
    @IBOutlet weak var profilePicbtn: UIButton!
    @IBOutlet weak var nameLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
