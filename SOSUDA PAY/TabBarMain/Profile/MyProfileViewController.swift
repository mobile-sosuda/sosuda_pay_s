//
//  MyProfileViewController.swift
//  SOSUDA PAY
//
//  Created by Saifur Rahman on 16/04/21.
//

import UIKit

class MyProfileViewController: UIViewController {
    var dictMod : UserDictModel?
    @IBOutlet weak var topNavBar: TopHeader!
    @IBOutlet weak var myTableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
                if  let dict = UserDefaults.standard.value(forKey: "data"){
                dictMod = try? JSONDecoder().decode(UserDictModel.self, from: dict as! Data)
                }
        myTableView.tableViewSetupWithViewC(ViewController: self, cellArrayToRegister: [LabelAndImageTableViewCell.getCellIdentifier(),ProfileHeaderTableViewCell.getCellIdentifier()])
        myTableView.delegate = self
        myTableView.dataSource = self
        // Do any additional setup after loading the view.
    }
    // MARK: - Navigation
}
extension MyProfileViewController:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        tableView.separatorStyle = .none
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "ProfileHeaderTableViewCell") as! ProfileHeaderTableViewCell
            cell.selectionStyle = .none
            cell.nameLabel.text = dictMod!.firstName + " " + dictMod!.lastName
            cell.mobilenumberLabel.text = dictMod!.userID
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "LabelAndImageTableViewCell") as! LabelAndImageTableViewCell
            cell.bottomLabelheightVonstant.constant = 0
            cell.errorImageViewRight.isHidden = true
            cell.switchButton.isHidden = true
            cell.bottomLabel.isHidden = true
            cell.lineSeperatorLeading.constant = 80
            if indexPath.row == 1{
                cell.profileDetailLabel.text = "My Address"
                    //self.dictMod.
            }else if indexPath.row == 2{
                cell.profileDetailLabel.text = "Change Password"
            }
                else if indexPath.row == 3{
                    cell.profileDetailLabel.text = "Disable Passcode"
                    cell.switchButton.isHidden = false
                    cell.switchButton.addTarget(self, action: #selector(switched(_:)), for: .valueChanged)

                }
                    else if indexPath.row == 4{
                        cell.profileDetailLabel.text = "Privacy Policy"
                        cell.lineSeperatorLeading.constant = 0
                    }
                        else if indexPath.row == 5{
                            cell.profileDetailLabel.text = "KYC Details"
                            cell.bottomLabel.isHidden = false
                            cell.errorImageViewRight.isHidden = true
                            cell.bottomLabel.text = "Please update KYC Details"
                            cell.bottomLabel.textColor = .red
                            cell.bottomLabelheightVonstant.constant = 25
                        }
                            else if indexPath.row == 6{
                                cell.profileDetailLabel.text = "Need Help?"
                            }
                                else if indexPath.row == 7{
                                    cell.profileDetailLabel.text = "FAQs"
                                }
                                    else if indexPath.row == 8{
                                        cell.profileDetailLabel.text = "Invite new User"
                                        cell.lineSeperatorLeading.constant = 0
                                    }else{
                                        cell.profileDetailLabel.text = "Logout"
                                    }
            
            
            return cell
        }
    }
    
    @objc func switched(_ sender:UISwitch){
        print(sender.isOn)
    }
}
