//
//  RightLeftButtonTableViewCell.swift
//  SOSUDA PAY
//
//  Created by Saifur Rahman on 11/04/21.
//

import UIKit

class RightLeftButtonTableViewCell: UITableViewCell {
    
    @IBOutlet weak var rightButton: UIButton!
    @IBOutlet weak var leftButton: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
