//
//  SwipeViewController.swift
//  BrainVire
//
//  Created by Saifur Rahman on 20/02/21.
//

import UIKit
import ContactsUI
class SwipeViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UIScrollViewDelegate{
    // MARK: - Outlets
    var oldOffset :CGFloat = 0
    @IBOutlet weak var collectionViewbottom: UICollectionView!
    
    @IBOutlet weak var topHeaderView: TopHeader!
    @IBOutlet weak var movingBar: UILabel!
    @IBOutlet weak var collectionViewTopbar: UICollectionView!
    var isSelectedTab :Int? = 1
    // MARK: - Variables
    var contacts = [CNContact]()
    static var vc :SwipeViewController!
    let topBarelements = ["Send","History","Deposit","Withdraw"]
//    let followers = ["Asheesh Chanchlani","Rafi Asari","Elizabeth Olsen","Sameer Anand"]
//    let following = ["Rafi Ansari","Prem","Razi Ullah","Harman Baweja","Prem Malhotra","GymLove"]
    var col = [ #colorLiteral(red: 0.09019608051, green: 0, blue: 0.3019607961, alpha: 1),#colorLiteral(red: 0.9607843161, green: 0.7058823705, blue: 0.200000003, alpha: 1),#colorLiteral(red: 0.3084011078, green: 0.5618229508, blue: 0, alpha: 1),#colorLiteral(red: 0.8078431487, green: 0.02745098062, blue: 0.3333333433, alpha: 1)]
    // MARK: - ViewController LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if #available(iOS 13.0, *) {
            topHeaderView.backButton.setImage(UIImage(systemName: "person.circle.fill"), for: .normal)
        } else {
            // Fallback on earlier versions
        }
        topHeaderView.backButtonWidth.constant = 60
        topHeaderView.backButton.tintColor = .lightGray
        topHeaderView.backgroundColor = #colorLiteral(red: 0.1957393885, green: 0.2087394297, blue: 0.7472228408, alpha: 1)
        topHeaderView.title.text = "Payments"
        collectionViewTopbar.collectionViewSetupWithViewC(ViewController: self, cellArrayToRegister: [SingleLabelCollectionViewCell.getCellIdentifier()])
        collectionViewbottom.collectionViewSetupWithViewC(ViewController: self, cellArrayToRegister: [TableCollectionViewCell.getCellIdentifier()])
        collectionViewTopbar.delegate = self
        collectionViewbottom.delegate = self
        collectionViewbottom.dataSource = self
        let layout2 = UICollectionViewFlowLayout()
        layout2.itemSize = CGSize(width: UIScreen.main.bounds.width/CGFloat(topBarelements.count), height: collectionViewTopbar.frame.height)
        layout2.scrollDirection = .horizontal
        layout2.minimumInteritemSpacing = 0
        layout2.minimumLineSpacing = 0
        collectionViewTopbar.collectionViewLayout = layout2
        
        let layout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        layout.itemSize = CGSize(width: collectionViewbottom.frame.width, height: collectionViewbottom.frame.height)
       // layout.shouldInvalidateLayout(forBoundsChange: <#T##CGRect#>)
        layout.scrollDirection = .horizontal
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = 0
        collectionViewbottom.collectionViewLayout = layout
        collectionViewbottom.isPagingEnabled = true
        movingBar.frame.size.width = UIScreen.main.bounds.width/4
        
    }
    override func viewWillLayoutSubviews() {
        collectionViewbottom.reloadData()
    }
    override func viewWillAppear(_ animated: Bool) {
        //collectionViewbottom.reloadData()
    }
    override func viewDidAppear(_ animated: Bool) {
       
    }
   

    // MARK: - CollectionviewDelegates
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
            return topBarelements.count
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == collectionViewTopbar{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SingleLabelCollectionViewCell", for: indexPath) as! SingleLabelCollectionViewCell
            cell.labelText.setTitle(topBarelements[indexPath.row], for: .normal)
            cell.labelText.tag = indexPath.row

            cell.labelText.addTarget(self, action: #selector(toptabclicked(_:)), for: .touchUpInside)
            return cell
        }else{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TableCollectionViewCell", for: indexPath) as! TableCollectionViewCell
           // cell.awakeFromNib()
           
          //  cell.tableViewInCollectionView.backgroundColor = col[indexPath.row]
            cell.tableViewInCollectionView.tag = indexPath.row
            cell.tag = indexPath.row
            cell.tableViewInCollectionView.restorationIdentifier = "\(indexPath.row)"
            cell.tableViewInCollectionView.separatorStyle = .none
//            cell.following = following
//            cell.followers = followers
            //cell.contacts = contacts
            
            cell.tableViewInCollectionView.reloadData()
            cell.tableViewInCollectionView.isUserInteractionEnabled = true
            cell.isUserInteractionEnabled = true
            cell.layoutSubviews()
            return cell
        }
    }
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        print("Actual indexpth", indexPath.row)
        cell.tag = indexPath.row
       // collectionView.reloadData()
    }
    @objc func toptabclicked(_ sender:UIButton){
        let rectIs = collectionViewbottom.layoutAttributesForItem(at: IndexPath(item: sender.tag , section: 0))?.frame
        collectionViewbottom.scrollRectToVisible(rectIs!, animated: true)
        isSelectedTab = sender.tag
        
       // collectionViewbottom.reloadItems(at: [IndexPath(row: sender.tag - 1, section: 0)])
     //  let c = collectionViewbottom.cellForItem(at: IndexPath(row: sender.tag - 1, section: 0)) as? TableCollectionViewCell
     //   c?.tableViewInCollectionView.reloadData()
    }
   
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
                let p = scrollView.contentOffset.x/4
            movingBar.transform = CGAffineTransform(translationX: p, y: 0)
        
    }
    func scrollViewDidEndScrollingAnimation(_ scrollView: UIScrollView) {
     //   print( collectionViewbottom.visibleCells.first?.tag)
    }
    func collectionView(_ collectionView: UICollectionView, didEndDisplaying cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
     //   print(indexPath.row)
    }
    // MARK: - tableviewDelegates


}

//    func fetchContacts(){
//        CNContactStore().requestAccess(for: .contacts) { (access, error) in
//          print("Access: \(access)")
//            if access == true{
//                let keys = [CNContactFormatter.descriptorForRequiredKeys(for: .fullName),CNContactPhoneNumbersKey as CNKeyDescriptor]
//                    let request = CNContactFetchRequest(keysToFetch: keys)
//
//                    let contactStore = CNContactStore()
//                    do {
//                        try contactStore.enumerateContacts(with: request) {
//                            (contact, stop) in
//                            // Array containing all unified contacts from everywhere
//                            print(contact)
//                            if let _ = contact.phoneNumbers.first{
//                            self.contacts.append(contact)
//                            }
//                        }
//                        DispatchQueue.main.async {
//                            self.collectionViewbottom.reloadData()
//
//                        }
//                    }
//                    catch {
//                        print("unable to fetch contacts")
//                    }
//            }
//        }
//
//    }
