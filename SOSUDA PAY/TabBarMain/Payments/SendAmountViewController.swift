//
//  SendAmountViewController.swift
//  SOSUDA PAY
//
//  Created by Saifur Rahman on 11/04/21.
//

import UIKit

class SendAmountViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var amountTableView: UITableView!
    @IBOutlet weak var topNavBar: TopHeader!
    var mobile:String?
    var name:String?
    var senderMobileNumberWithoutCountryCode:String?
    var dict :TransactionConversionModel?
    var amount: String?
    var msg = String()
    var countryAlphaCode:String?
    override func viewDidLoad() {
        super.viewDidLoad()
        topNavBar.title.text = "Payments"
        topNavBar.title.textColor = .white
        topNavBar.backButton.setImage(UIImage(systemName: "chevron.backward"), for: .normal)
        topNavBar.backButton.tintColor = .white
        topNavBar.backButtonWidth.constant = 60
        amountTableView.tableViewSetupWithViewC(ViewController: self, cellArrayToRegister: [SendAmountTableViewCell.getCellIdentifier(),RightLeftButtonTableViewCell.getCellIdentifier(),RightAndLeftLabelTableViewCell.getCellIdentifier(),SingleButtonTableViewCell.getCellIdentifier()])
        amountTableView.separatorStyle = .none
        amountTableView.delegate = self
        amountTableView.dataSource = self
    }
    @objc func sendMoney(){
        
        self.view.endEditing(true)
        if Reachability.isConnectedToNetwork(){
            //    let carddata = self.arrayOfCards[sender.tag]
            if  let dict = UserDefaults.standard.value(forKey: "data"){
                let dictMod = try? JSONDecoder().decode(UserDictModel.self, from: dict as! Data)
                
                let param = ["senderUserID": dictMod!.userID,
                             "senderMoobileNo":dictMod!.userID,
                             "senderSubscriptionId": dictMod!.subscriptionID,
                             "senderCountryAlphaCode": dictMod!.countryAlphaCode,
                             "sendingAmount": amount!,
                             "receiverMoobileNo":self.senderMobileNumberWithoutCountryCode!,
                             "recevierCalingCode":self.dict!.data.reciverCountry.callingCodes,
                             "txnAmout":self.dict!.data.sendingAmount,
                             "message":msg,
                             "txnAmountAfterAllConversionAtReciverEnd":self.dict!.data.finalAmountAtReciverSide,
                             "transationDetailsId":self.dict!.data.transationDetailsID,
                             "reciverUserId": mobile!,
                             "receiverCountryAlphaCode": dictMod!.countryAlphaCode] as [String : Any]
                let urlString = (P2P_TRANSACTION as NSString)
                
                ResponseFromApi.shared.postRequestForParameters(url: urlString as String, params: param, header: ["Authorization" : "Bearer \(dictMod!.token)","Content-Type": "application/json"]) { (Response,dat,isSuccess) in
                    if isSuccess{
                        print(Response)
                        if let stat = (Response as? NSDictionary)?["status"] as? Int ,stat == 1{
                                DispatchQueue.main.async {
                                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "SuccessfullTransactionViewController") as! SuccessfullTransactionViewController
                                    
                                    vc.amount = "\(self.dict!.data.sendingAmount)"
                                   
                                    self.navigationController?.pushViewController(vc, animated: true)
                                   //po p self.amountTableView.reloadData()
                                }
                        }
                       
                    }
                    // self.getCards()
                }
            }
        }
    }
    @objc func cancelTransaction(){
        dict = nil
        amount = nil
        msg = ""
        amountTableView.reloadData()
    }
    @objc func convertTransaction(){
        self.view.endEditing(true)
        if Reachability.isConnectedToNetwork(){
            //    let carddata = self.arrayOfCards[sender.tag]
            if  let dict = UserDefaults.standard.value(forKey: "data"){
                let dictMod = try? JSONDecoder().decode(UserDictModel.self, from: dict as! Data)
                
                let param = ["senderUserId": dictMod!.userID,
                             "senderSubscriptionId": dictMod!.subscriptionID,
                             "senderCountryAlphaCode": dictMod!.countryAlphaCode,
                             "sendingAmount": amount!,
                             "reciverUserId": mobile!,
                             "reciverCountryAlphaCode": dictMod!.countryAlphaCode] as [String : Any]
                let urlString = (GET_TRANSACTION_CONVERSION as NSString)
                
                ResponseFromApi.shared.postRequestForParameters(url: urlString as String, params: param, header: ["Authorization" : "Bearer \(dictMod!.token)","Content-Type": "application/json"]) { (Response,dat,isSuccess) in
                    if isSuccess{
                        print(Response)
                        do{
                            let conversionModel = try? JSONDecoder().decode(TransactionConversionModel.self, from: dat as! Data)
                            if conversionModel != nil{
                                self.dict = conversionModel
                                DispatchQueue.main.async {
                                    self.amountTableView.reloadData()
                                }
                            }
                        }catch{
                            print(error)
                        }
                    }
                    // self.getCards()
                }
            }
        }
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField.tag == 1{
            if !(textField.text?.trimmingCharacters(in: .whitespaces).isEmpty)!{
                if let _ = Double(textField.text!){
                    amount = textField.text!
                }
                
            }
        }else if textField.tag == 2{
            
            if !(textField.text?.trimmingCharacters(in: .whitespaces).isEmpty)!{
                msg = textField.text!.trimmingCharacters(in: .whitespaces)
            }
        }
    }
    // MARK: - Navigation
    
}
extension SendAmountViewController:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let _ = dict{
            return 15
        }else{
            return 1
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "SendAmountTableViewCell") as! SendAmountTableViewCell
            cell.nameField.text = name
            cell.numberField.text = mobile
            if amount != nil{
                cell.amountTextField.text = amount!
                cell.messageTextField.text = msg
            }
            cell.amountTextField.tag = 1
            cell.amountTextField.delegate = self
            cell.messageTextField.tag = 2
            cell.messageTextField.delegate = self
            cell.sendMoneyButton.addTarget(self, action: #selector(convertTransaction), for: .touchUpInside)
            cell.selectionStyle = .none
            return cell
        }else if indexPath.row == 1{
            let cell = tableView.dequeueReusableCell(withIdentifier: "SingleButtonTableViewCell") as! SingleButtonTableViewCell
            cell.centerButton.backgroundColor = .clear
            cell.centerButton.setTitleColor(.black, for: .normal)
            cell.centerButton.setTitle("Transaction Details", for: .normal)
            cell.selectionStyle = .none
            return cell
        }else if indexPath.row == 14{
            let cell = tableView.dequeueReusableCell(withIdentifier: "RightLeftButtonTableViewCell") as! RightLeftButtonTableViewCell
            cell.leftButton.addTarget(self, action: #selector(sendMoney), for: .touchUpInside)
            cell.rightButton.addTarget(self, action: #selector(cancelTransaction), for: .touchUpInside)
            cell.selectionStyle = .none
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "RightAndLeftLabelTableViewCell") as! RightAndLeftLabelTableViewCell
            if indexPath.row == 2{
                cell.leftLabel.text = "Subscription"
                cell.rightLabel.text = dict!.data.subscriptionDetails.name
                cell.leftLabel.textColor = .black
                cell.rightLabel.textColor = cell.leftLabel.textColor
            } else if indexPath.row == 3{
                cell.leftLabel.text = "Available Amount Transaction*"
                cell.rightLabel.text = "\(self.dict!.data.reciverCountry.symbol)8000"
                cell.leftLabel.textColor = .green
                cell.rightLabel.textColor = cell.leftLabel.textColor
            }
                else if indexPath.row == 4{
                    cell.leftLabel.text = "Available Allowed Transaction*"
                    cell.rightLabel.text = "\(self.dict!.data.subscriptionDetails.maxHitOfDay)"
                    cell.leftLabel.textColor = .green
                    cell.rightLabel.textColor = cell.leftLabel.textColor
            }
                    else if indexPath.row == 5{
                        cell.leftLabel.text = "Maximum Allowed Amount"
                        cell.rightLabel.text = "\(self.dict!.data.reciverCountry.symbol)\(self.dict!.data.subscriptionDetails.maxLimitOfDay)"
                        cell.leftLabel.textColor = .black
                        cell.rightLabel.textColor = cell.leftLabel.textColor
                    }
                        else if indexPath.row == 6{
                            cell.leftLabel.text = "Maximum Allowed Transactions"
                            cell.rightLabel.text = "\(self.dict!.data.perDayLimit.tolalNumberOfHit)"
                            cell.leftLabel.textColor = .black
                            cell.rightLabel.textColor = cell.leftLabel.textColor
                    }
                            else if indexPath.row == 7{
                                cell.leftLabel.text = "Transaction Amount"
                                cell.rightLabel.text = "\(self.dict!.data.reciverCountry.symbol)\(self.dict!.data.finalAmountAtReciverSide)"
                                cell.leftLabel.textColor = .black
                                cell.rightLabel.textColor = cell.leftLabel.textColor
                            }
                                else if indexPath.row == 8{
                                    cell.leftLabel.text = "Transaction Charges"
                                    cell.rightLabel.text = "\(self.dict!.data.reciverCountry.symbol)\(self.dict!.data.transactionCharge)"
                                    cell.leftLabel.textColor = .red
                                    cell.rightLabel.textColor = cell.leftLabel.textColor
                            }
                                    else if indexPath.row == 9{
                                        cell.leftLabel.text = "Service Charge"
                                        cell.rightLabel.text = "\(self.dict!.data.reciverCountry.symbol)\(self.dict!.data.serviceCharge)"
                                        cell.leftLabel.textColor = .red
                                        cell.rightLabel.textColor = cell.leftLabel.textColor
                                    }
                                        else if indexPath.row == 10{
                                            cell.leftLabel.text = "Final Transaction Amount"
                                            cell.rightLabel.text = "\(self.dict!.data.reciverCountry.symbol)\(self.dict!.data.recivingAmountbeforExchange)"
                                            cell.leftLabel.textColor = .black
                                            cell.rightLabel.textColor = cell.leftLabel.textColor
                                    }
                                            else if indexPath.row == 11{
                                                cell.leftLabel.text = " Exchange Rate \(self.dict!.data.senderCountry.symbol) to \(self.dict!.data.reciverCountry.symbol)"
                                                cell.rightLabel.text = "\(self.dict!.data.excengeRate)"
                                                cell.leftLabel.textColor = .black
                                                cell.rightLabel.textColor = cell.leftLabel.textColor
                                            }
                                                else if indexPath.row == 12{
                                                    cell.leftLabel.text = "Recieving Amount"
                                                    cell.rightLabel.text = "\(self.dict!.data.finalAmountAtReciverSide)"
                                                    cell.leftLabel.textColor = .black
                                                    cell.rightLabel.textColor = cell.leftLabel.textColor
                                                }else{
                                                    cell.leftLabel.text = "*Transaction and Amount limit will be automatically reset on starting the new day."
                                                    cell.rightLabel.text = ""
                                                    cell.leftLabel.textColor = .green
                                                    cell.rightLabel.textColor = cell.leftLabel.textColor
                                                }
            
            

            cell.selectionStyle = .none
            return cell
        }
    }
    
    
}
