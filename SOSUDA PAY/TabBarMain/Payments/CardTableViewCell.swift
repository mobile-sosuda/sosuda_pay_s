//
//  CardTableViewCell.swift
//  SOSUDA PAY
//
//  Created by Saifur Rahman on 03/04/21.
//

import UIKit

class CardTableViewCell: UITableViewCell {

    @IBOutlet weak var deleteButtton: UIButton!
    @IBOutlet weak var backGroundViewCard: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        backGroundViewCard.layer.cornerRadius = 10
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
