//
//  SendAmountTableViewCell.swift
//  SOSUDA PAY
//
//  Created by Saifur Rahman on 11/04/21.
//

import UIKit

class SendAmountTableViewCell: UITableViewCell {
    @IBOutlet weak var amountTextField: Designabletextfield!
    
    @IBOutlet weak var sendMoneyButton: DesignableButton!
    @IBOutlet weak var messageTextField: Designabletextfield!
    @IBOutlet weak var nameField: UILabel!
    @IBOutlet weak var numberField: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
