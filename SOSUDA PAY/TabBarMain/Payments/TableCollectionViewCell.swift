//
//  TableCollectionViewCell.swift
//  BrainVire
//
//  Created by Saifur Rahman on 20/02/21.
//

import UIKit
import Contacts
import ContactsUI
class TableCollectionViewCell: UICollectionViewCell,UITableViewDataSource,UITableViewDelegate,CNContactPickerDelegate,UITextFieldDelegate {
    var followers :[String]?
    var following :[String]?
    var isSelectingCountry = Bool()
    var arryOfCountry = NSArray()
    var arryOfTransaction = [Transaction]()
    var isLoadingTransactions = Bool()
    var countryName = String()
    var countryCode = String()
    var countryImage = UIImage()
    var contacts = [CNContact]()
    var isSelectingContacts = Bool()
    var pageNumber = 1
    var mobileNumber:String?
    @IBOutlet weak var tableViewInCollectionView: UITableView!
    override func layoutSubviews() {
//        isSelectingCountry = false
//        isSelectingContacts = false
//        isLoadingTransactions = false
        tableViewInCollectionView.register(UINib(nibName: "FollowTableViewCell", bundle: nil), forCellReuseIdentifier: "FollowTableViewCell")
        tableViewInCollectionView.register(UINib(nibName: "SendMoneyTableViewCell", bundle: nil), forCellReuseIdentifier: "SendMoneyTableViewCell")
        tableViewInCollectionView.register(UINib(nibName: "CountryTableViewCell", bundle: nil), forCellReuseIdentifier: "CountryTableViewCell")
        tableViewInCollectionView.register(UINib(nibName: "TransactionTableViewCell", bundle: nil), forCellReuseIdentifier: "TransactionTableViewCell")
        //(FollowTableViewCell.self, forCellReuseIdentifier: "FollowTableViewCell")
        tableViewInCollectionView.delegate = self
        tableViewInCollectionView.dataSource = self
        self.tableViewInCollectionView.reloadData()
        self.isUserInteractionEnabled = true
        if tableViewInCollectionView.tag == 0{
            isLoadingTransactions = false

        ResponseFromApi.shared.getRequestForParameters(url: getCountryList) { (resp, isSuccess) in
          //  print(resp,isSuccess)
            if isSuccess{
                if let _ = (resp as! NSDictionary)["data"] as? NSArray{
                    self.arryOfCountry = ((resp as! NSDictionary)["data"] as! NSArray)
                    //                    self.countryTableView.delegate = self
                    //                    self.countryTableView.dataSource = self
                    //                    self.countryTableView.reloadData()
                }
            }
            DispatchQueue.main.async {
                self.tableViewInCollectionView.reloadData()

            }

        }
        }
        if tableViewInCollectionView.tag == 1{
            isSelectingCountry = false
            isSelectingContacts = false
            pageNumber = 1
            getTransactions(pageNumber: "\(pageNumber)", pageSize: "10")
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
       // tableViewInCollectionView.tag = self.tag
   
        
    }
    // MARK: - Transaction History

    func getTransactions(pageNumber:String,pageSize:String){
        if !isLoadingTransactions{
        if  let dict = UserDefaults.standard.value(forKey: "data"){
            let dictMod = try? JSONDecoder().decode(UserDictModel.self, from: dict as! Data)
            if let _ = dictMod{
                self.isLoadingTransactions = true
                let urlString = (TRANSACTION_HISTORY as NSString).appendingPathComponent("?userId=\(dictMod!.userID)$\(pageNumber)$\(pageSize)")
                ResponseFromApi.shared.getRequestWithHeader(url: urlString ,header: ["Authorization" : "Bearer \(dictMod!.token)","Content-Type": "application/json"]) { (Response,dat,isSuccess) in
                    self.isLoadingTransactions = false
                    if isSuccess{
                  //      print(Response)
                        let dictAccount1 = try? JSONDecoder().decode(AllTransactions.self, from: dat as! Data)
                        if dictAccount1?.data != nil{
                            self.pageNumber += 1
                            self.arryOfTransaction = self.arryOfTransaction + dictAccount1!.data
                            self.tableViewInCollectionView.reloadData()
                            // self.recentTransactionsTable.reloadData()
                        }
                      //  print(dictAccount1)
                    }
                }
            }
        }
    }
    }


// MARK: - Tabledelegates Extension
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    // MARK: - tableviewDelegates
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView.tag == 0{
            if !isSelectingCountry{
                return 1
            }else if isSelectingContacts{
                return contacts.count
            } else{
                return arryOfCountry.count
            }
        }else if tableView.tag == 1{
            return arryOfTransaction.count
        } else if tableView.tag == 2{
            return 0
        }else{
            return 0
        }
    }
    
        
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView.tag == 0{
            if !isSelectingCountry{
                let cell = tableView.dequeueReusableCell(withIdentifier: "SendMoneyTableViewCell") as! SendMoneyTableViewCell
                cell.isUserInteractionEnabled = true
                cell.mobileTxtField.isUserInteractionEnabled = true
                cell.mobileTxtField.isEnabled = true
                if mobileNumber != nil{
                    cell.mobileTxtField.text = mobileNumber!
                }
                cell.mobileTxtField.textColor = #colorLiteral(red: 0.1957393885, green: 0.2087394297, blue: 0.7472228408, alpha: 1)
                cell.mobileTxtField.delegate = self
                cell.mobileTxtField.rightView?.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(selectContact(_:))))
                if !countryName.isEmpty{
                    cell.countryName.text = countryName
                    cell.countryCode.text = countryCode
                    cell.countryFlag.image = countryImage
                }
                cell.selectCountyBtn.addTarget(self, action: #selector(clicked), for: .touchUpInside)
                cell.sendbutton.addTarget(self, action: #selector(sendButtonAction), for: .touchUpInside)
                return cell
            }else if isSelectingContacts{
                let cell = tableView.dequeueReusableCell(withIdentifier: "CountryTableViewCell") as! CountryTableViewCell
                cell.countrynameLabel.text = contacts[indexPath.row].givenName
                cell.countrynameLabel.font = UIFont.systemFont(ofSize: 12)
                //            for number: CNLabeledValue in contact.phoneNumbers {
                //                    print(number.value)
                //                }
                //
                let number: CNLabeledValue = contacts[indexPath.row].phoneNumbers.first!
                let digits = number.value.value(forKey: "digits") as? String
                cell.countryCodeLabel.text = digits
                //contacts[indexPath.row].phoneNumbers.first
                cell.countryCodeLabel.font = UIFont.systemFont(ofSize: 10)
                // cell.countryCodeLabel.text = "\(contacts[indexPath.row].phoneNumbers.first)"
                return cell
            }else{
                let cell = tableView.dequeueReusableCell(withIdentifier: "CountryTableViewCell") as! CountryTableViewCell
                cell.countrynameLabel.text = (self.arryOfCountry[indexPath.row] as! NSDictionary)["name"] as? String
                cell.countryCodeLabel.text = (self.arryOfCountry[indexPath.row] as! NSDictionary)["callingCodes"]
                    as!String
                return cell
            }
            
        }else  if tableView.tag == 1{
            let cell = tableView.dequeueReusableCell(withIdentifier: "TransactionTableViewCell") as! TransactionTableViewCell
            let startingletter = self.arryOfTransaction[indexPath.row].txnSourceName.first!.lowercased()
            cell.profileImage.image = UIImage(systemName: "\(startingletter).circle.fill")
            let theName = self.arryOfTransaction[indexPath.row].txnSourceName.split(separator: " ")
            cell.profilename.text = theName.first! + " " + theName.last!
            if self.arryOfTransaction[indexPath.row].crDR == "CR"{
                cell.amount.text = "+\(self.arryOfTransaction[indexPath.row].txnAmount)"
                cell.amount.textColor = .green
                cell.dateOfTransaction.text = ""
            }else{
                cell.amount.text = "-\(self.arryOfTransaction[indexPath.row].txnAmount)"
                cell.amount.textColor = .red
            }
            cell.contentView.backgroundColor = .red
            return cell
            
        }else if tableView.tag == 2{
            let cell = tableView.dequeueReusableCell(withIdentifier: "CountryTableViewCell") as! CountryTableViewCell
            
            // cell.countrynameLabel.text = (self.arryOfCountry[indexPath.row] as! NSDictionary)["name"] as!String
            // cell.countryCodeLabel.text = (self.arryOfCountry[indexPath.row] as! NSDictionary)["callingCodes"]
            //   as! String
            return cell
        }else{
            return UITableViewCell().self
        }
        
    }
    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if tableView.tag == 1{
            if indexPath.row == (arryOfTransaction.count - 1){
             //   getTransactions(pageNumber: "\(pageNumber)", pageSize: "10")
            }
        }
    }
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView == tableViewInCollectionView && tableViewInCollectionView.tag == 1{
        let offsetY = scrollView.contentOffset.y
        let contentHeight = scrollView.contentSize.height
        
        if offsetY > contentHeight - scrollView.frame.size.height {
            getTransactions(pageNumber: "\(pageNumber)", pageSize: "10")

        }
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        mobileNumber = textField.text?.trimmingCharacters(in: .whitespaces)
    }
    
    @objc func sendButtonAction(){
        findViewController()?.view.endEditing(true)
        if mobileNumber != nil{
            if mobileNumber!.count >= 6{
                confirmNumber()
            }
        }
    }
    
    func confirmNumber(){
        if  let dict = UserDefaults.standard.value(forKey: "data"){
            let dictMod = try? JSONDecoder().decode(UserDictModel.self, from: dict as! Data)
            if let _ = dictMod{
                
                let urlString = (VERIFYUSERID as NSString).appendingPathComponent("/\(countryCode+mobileNumber!)")
                ResponseFromApi.shared.getRequestWithHeader(url: urlString ,header: ["Authorization" : "Bearer \(dictMod!.token)","Content-Type": "application/json"]) { (Response,dat,isSuccess) in
                    if isSuccess{
                        if let resp_ = (Response as! NSDictionary)["data"] as? NSNull{
                            Alert.showBasic(title: "Error", message: "No account registered numwith this number exists", vc: self.findViewController()!)
                            
                        }else{
                            if let resp_ = (Response as! NSDictionary)["data"] as? NSDictionary{
                                DispatchQueue.main.async {
                                    let vc = self.findViewController()?.storyboard?.instantiateViewController(withIdentifier: "SendAmountViewController") as! SendAmountViewController
                                    vc.name = resp_["firstName"] as? String
                                    vc.mobile = resp_["userId"] as? String
                                    vc.senderMobileNumberWithoutCountryCode = self.mobileNumber!
                                    vc.countryAlphaCode = (resp_["countryAlphaCode"] as? String)?.trimmingCharacters(in: .whitespaces)
                                    self.findViewController()?.navigationController?.pushViewController(vc, animated: true)
                                }
                            }
                        }
                        // let dictAccount = try? JSONDecoder().decode(AccountInfo.self, from: dat!)
                        // self.accountBalance = Double(((dictAccount!).data[0].balance))
                    }else{
                        Alert.showBasic(title: "Error", message: "Please check the number again", vc: self.findViewController()!)
                        
                    }
                }
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        countryName = (self.arryOfCountry[indexPath.row] as! NSDictionary)["name"] as!String
        countryCode = (self.arryOfCountry[indexPath.row] as! NSDictionary)["callingCodes"] as! String
        let selectedDic = self.arryOfCountry[indexPath.row] as! NSDictionary
        // countryTableView.isHidden = true
        let urlIs = (getFlagImage as NSString).appendingPathComponent(selectedDic["flag"] as! String)
        
        if let url = URL(string: urlIs) {
            let task = URLSession.shared.dataTask(with: url) { data, response, error in
                guard let data = data, error == nil else { return }
                
                DispatchQueue.main.async { /// execute on main thread
                    self.countryImage = UIImage(data: data)!
                    self.isSelectingCountry = false
                    self.tableViewInCollectionView.reloadData()
                }
            }
            
            task.resume()
        }
    }
    @objc func clicked(){
        isSelectingCountry = true
        tableViewInCollectionView.reloadData()
        print("clicked")
    }
    @objc func selectContact(_ sender:UITapGestureRecognizer){
        print("country Select")
        if isSelectingContacts{
            //  isSelectingContacts = false
        }else{
            isSelectingContacts = true
            onClickPickContact()
        }
        //  tableViewInCollectionView.reloadData()
        
    }
    // MARK: - Contact Picker & its Delegates
    
    func onClickPickContact(){
        
        
        let contactPicker = CNContactPickerViewController()
        contactPicker.delegate = self
        contactPicker.displayedPropertyKeys =
            [CNContactGivenNameKey
             , CNContactPhoneNumbersKey]
        self.findViewController()?.present(contactPicker, animated: true, completion: nil)
        
    }
    func contactPicker(_ picker: CNContactPickerViewController,
                       didSelect contactProperty: CNContactProperty) {
        
    }
    func contactPicker(_ picker: CNContactPickerViewController, didSelect contact: CNContact) {
        // You can fetch selected name and number in the following way
        isSelectingContacts = false
        // user name
        let userName:String = contact.givenName
        
        // user phone number
        if !contact.phoneNumbers.isEmpty{
            let userPhoneNumbers:[CNLabeledValue<CNPhoneNumber>] = contact.phoneNumbers
            let firstPhoneNumber:CNPhoneNumber = userPhoneNumbers[0].value
            
            
            // user phone number string
            let primaryPhoneNumberStr:String = firstPhoneNumber.stringValue
            mobileNumber = primaryPhoneNumberStr
            tableViewInCollectionView.reloadData()
            // print(primaryPhoneNumberStr)
        }else{
            picker.dismiss(animated: true, completion: nil)
            Alert.showBasic(title: "Error", message: "No number exists", vc: findViewController()!)
        }
    }
    
    func contactPickerDidCancel(_ picker: CNContactPickerViewController) {
        isSelectingContacts = false
    }
}

