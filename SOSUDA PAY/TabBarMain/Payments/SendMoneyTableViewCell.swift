//
//  SendMoneyTableViewCell.swift
//  SOSUDA PAY
//
//  Created by Saifur Rahman on 24/03/21.
//

import UIKit
import DTTextField
class SendMoneyTableViewCell: UITableViewCell {

    @IBOutlet weak var sendbutton: DesignableButton!
    @IBOutlet weak var mobileTxtField: DTTextField!
    @IBOutlet weak var selectCountyBtn: DesignableButton!
    @IBOutlet weak var countryFlag: UIImageView!
    @IBOutlet weak var countryName: UILabel!
    @IBOutlet weak var countryCode: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        
        mobileTxtField.backgroundColor = .yellow
        mobileTxtField.borderStyle = .none
//        mobileTxtField.borderColor = #colorLiteral(red: 0.2842571437, green: 0.7277087569, blue: 0.9657751918, alpha: 1)
        mobileTxtField.errorMessage = "Invalid Number"
        mobileTxtField.placeholder = "Mobile Number"
        mobileTxtField.placeholderColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
        mobileTxtField.floatPlaceholderColor = #colorLiteral(red: 0.2842571437, green: 0.7277087569, blue: 0.9657751918, alpha: 1)
        mobileTxtField.floatPlaceholderActiveColor = #colorLiteral(red: 0.1957393885, green: 0.2087394297, blue: 0.7472228408, alpha: 1)
        mobileTxtField.isUserInteractionEnabled = true
        mobileTxtField.animateFloatPlaceholder = true
        mobileTxtField.borderWidth = 0
        mobileTxtField.paddingYFloatLabel = 10
        mobileTxtField.paddingXFloatLabel = 25
        mobileTxtField.floatPlaceholderFont = UIFont.systemFont(ofSize: 14, weight: .medium)
        //txtField.addPadding(UITextField.PaddingSide.left(20))
        let rightImgView = UIView.init(frame: CGRect(x: mobileTxtField.frame.width - 65, y: 10, width: 50, height: 50))
        let imag = UIImageView.init(frame: CGRect(x: 15, y: 25, width: 20, height: 20))
        if #available(iOS 13.0, *) {
            imag.image = UIImage(systemName: "a.book.closed")
        } else {
            // Fallback on earlier versions
        }
        rightImgView.addSubview(imag)
        mobileTxtField.rightView = rightImgView
        mobileTxtField.rightViewMode = .always
        
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
//    @IBAction func selectCountry(_ sender: Any) {
//        print("select now country")
//       
//    }
}
