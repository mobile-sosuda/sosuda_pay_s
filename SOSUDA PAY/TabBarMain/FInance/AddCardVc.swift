import UIKit
import Stripe
//import SwiftyJSON
import Alamofire
 
class AddCardVC: UIViewController, STPPaymentCardTextFieldDelegate {
    @IBOutlet weak var topNavBar: TopHeader!
//    var isfromVC : Bool!
//    @IBOutlet weak var cardDetailView: UIView!
//    @IBOutlet weak var cardNumberTF: UITextField!
//    @IBOutlet weak var nameTF: UITextField!
//    @IBOutlet weak var mmTF: UITextField!
//    @IBOutlet var titleLbl: UILabel!
//    @IBOutlet var nameLbl: UILabel!
//    @IBOutlet var cardNumberLbl: UILabel!
//    @IBOutlet var cvvNoLbl: UILabel!
//    @IBOutlet var expiryLbl: UILabel!
//    @IBOutlet weak var cardImage: UIImageView!
//    @IBOutlet weak var cvvNOTF: UITextField!
//    @IBOutlet weak var yearTF: UITextField!
//    @IBOutlet weak var addNewBtn: UIButton!
//    @IBOutlet weak var topView: UIView!
//    var card = STPCardParams()
//    var  checkMandatoryCard = false
    var cardTextField = STPPaymentCardTextField()
//    var isfromHome : Bool!
//    var uiImage : UIImage!
    override func viewDidLoad() {
        super.viewDidLoad()
        topNavBar.logoutButton.isHidden = false
        topNavBar.logoutButton.setImage(nil, for: .normal)
        topNavBar.logoutButton.setTitle("Add", for: .normal)
        topNavBar.logoutButton.setTitleColor(.white, for: .normal)
        topNavBar.backButton.setImage(UIImage(systemName: "chevron.backward"), for: .normal)
        topNavBar.backButton.tintColor = .white
        topNavBar.backButtonWidth.constant = 60
        topNavBar.logoutButton.removeTarget(nil, action: nil, for: .allEvents)
        topNavBar.logoutButton.addTarget(self, action: #selector(done), for: .touchUpInside)
        topNavBar.logoutButton.isUserInteractionEnabled = true

//        // Do any additional setup after loading the view.
//        self.topView.roundCorners(cornerRadius: 50.0)
//        Utils.setGradientBackgroundLeftToRight(btn: addNewBtn)
//        addNewBtn.roundCornerWithPosition(cornerRadius: 25.0, positionMask: [.layerMinXMinYCorner, .layerMaxXMinYCorner])
//
//        titleLbl.text = "ADD NEW CARD".localized
//        cardNumberLbl.text = "Card Number".localized
//        cardNumberTF.placeholder = "Card Number".localized
//        nameLbl.text = "nameText".localized
//        nameTF.placeholder = "nameText".localized
//        cvvNoLbl.text = "CVV No".localized
//        cvvNOTF.placeholder = "CVV".localized
//        expiryLbl.text = "Expiry Date".localized
//        mmTF.placeholder = "MM".localized
//        yearTF.placeholder = "Year".localized
//        addNewBtn.setTitle("ADD NOW".localized, for: .normal)
//
       cardTextField = STPPaymentCardTextField(frame: CGRect(x: 0, y: 100, width: self.view.frame.width, height: 300))
        //cardDetailView.layer.cornerRadius = 20.0
       cardTextField.layer.borderColor = UIColor.clear.cgColor
        cardTextField.clipsToBounds = true
       cardTextField.textColor = UIColor.blue
        cardTextField.font = UIFont.systemFont(ofSize: 18)
        self.cardTextField.delegate = self
        self.view.addSubview(self.cardTextField)
       
//        let butonPro = UIButton(frame: (x: 16, y: 150, width: self.view.frame.width - 32, height: 70))
//        butonPro.backgroundColor = #colorLiteral(red: 0.2842571437, green: 0.7277087569, blue: 0.9657751918, alpha: 1)
//        butonPro.setTitle("Proceed", for: .normal)
//        cardNumberTF.delegate = self
//        cardNumberTF.keyboardType = .numberPad
//        nameTF.keyboardType = .emailAddress
//        cvvNOTF.keyboardType = .numberPad
//        nameTF.delegate = self
//        cvvNOTF.delegate = self
//        cardNumberTF.becomeFirstResponder()
//
//        cardNumberTF.addTarget(self, action: #selector(didChangeText(textField:)), for: .editingChanged)
//
//        let expiryMonthPicker = MonthYearPickerView()
//        expiryMonthPicker.isMonthValue = true
//        expiryMonthPicker.onDateSelected = { (month: Int, year: Int) in
//            let string = String(format: "%02d/%d", month, year)
//            NSLog(string) // should show something like 05/2015
//            self.mmTF.text = String(format: "%02d", month)
//        }
//
//        let expiryYearPicker = MonthYearPickerView()
//        expiryYearPicker.isMonthValue = false
//        expiryYearPicker.onDateSelected = { (month: Int, year: Int) in
//            let string = String(format: "%02d/%d", month, year)
//            NSLog(string) // should show something like 05/2015
//            self.yearTF.text = String(format: "%d", year)
//        }
//
//        self.mmTF.inputView   = expiryMonthPicker
//        self.mmTF.text = Date().dateStringFromDate(format: "MM")
//        self.yearTF.inputView   = expiryYearPicker
//        self.yearTF.text = Date().dateStringFromDate(format: "yyyy")
//        topView.clipsToBounds = true
//        self.topView.roundCorners(cornerRadius: 50.0)
//
//        Utils.methodForTextfieldWithBorderAndRadious(textField: cardNumberTF, width: 0.50, radious: 20)
//        Utils.methodForTextfieldWithBorderAndRadious(textField: mmTF, width: 0.50, radious: 20)
//        Utils.methodForTextfieldWithBorderAndRadious(textField: yearTF, width: 0.50, radious: 20)
//         Utils.methodForTextfieldWithBorderAndRadious(textField: nameTF, width: 0.50, radious: 20)
//         Utils.methodForTextfieldWithBorderAndRadious(textField: cvvNOTF, width: 0.50, radious: 20)
//
//        cardNumberTF.setLeftPaddingPoints(10)
//        cardNumberTF.setRightPaddingPoints(10)
//        mmTF.setLeftPaddingPoints(10)
//        mmTF.setRightPaddingPoints(10)
//        yearTF.setLeftPaddingPoints(10)
//        yearTF.setRightPaddingPoints(10)
//        nameTF.setLeftPaddingPoints(10)
//        nameTF.setRightPaddingPoints(10)
//        cvvNOTF.setLeftPaddingPoints(10)
//        cvvNOTF.setRightPaddingPoints(10)
//
//        mmTF.setRightIcon(UIImage(named: "Group 2100")!, padding: 10, size: 30)
//        yearTF.setRightIcon(UIImage(named: "Group 2100")!, padding: 10, size: 30)
//
        
}
    @objc func done(){
        if cardTextField.isValid{
            print("valid")
            let cardParams = STPCardParams()
             cardParams.number = cardTextField.cardNumber
            cardParams.expMonth = UInt(cardTextField.expirationMonth)
            cardParams.expYear = UInt(cardTextField.expirationYear)
            cardParams.cvc = cardTextField.cvc
            STPAPIClient.shared.createToken(withCard: cardParams) { (token: STPToken?, error: Error?) in
                     guard let token = token, error == nil else {
                        DispatchQueue.main.async {
                            Alert.showBasic(title: "Error", message: "Cannot Add the card", vc: self)
                        }
                         return
                     }
                print(token)
                self.addTheCard("\(token)")
                  //   self.dictPayData["stripe_token"] = token.tokenId
                 //    print(self.dictPayData)
    
                 }
            
        }else{
            print("not valid")
        }
    }
//
    func addTheCard(_ token:String){
        if  let dict = UserDefaults.standard.value(forKey: "data"){
        let dictMod = try? JSONDecoder().decode(UserDictModel.self, from: dict as! Data)
            var urlString = (STRIPEADDCARD as NSString).appendingPathComponent("?customerId=\(dictMod!.stripeCustomerID)&cardId=\(token)")
            ResponseFromApi.shared.postRequestWithHeader(url: urlString ,header: ["Authorization" : "Bearer \(dictMod!.token)","Content-Type": "application/json"]) { (Response,dat,isSuccess) in
            if isSuccess{
                if (Response as! NSDictionary)["status"] as! Int ==  1{
                    self.navigationController?.popViewControllerWithHandler {
                        Alert.showBasic(title: "Stripe", message: "Card Added Successfully", vc: self)
                    }
                }
                print(Response)
               // let dictAccount = try? JSONDecoder().decode(AccountInfo.self, from: dat as! Data)
                //self.accountBalance = Double(((dictAccount!).data[0].balance))
            }
        }
        }
        
    }
    @objc func didChangeText(textField:UITextField) {

        textField.text = self.modifyCreditCardString(creditCardString: textField.text!)
        if textField.text!.replacingOccurrences(of: " ", with: "").count == 14{
            print("working")
            //let param = STPCardParams()
            //param.number = "\(textField.text!)"
            //cardTextField.cardParams = param
            let param = STPPaymentMethodCardParams()
            param.number = "\(textField.text!)"
            cardTextField.cardParams = param
            cardTextField.delegate?.paymentCardTextFieldDidEndEditingNumber!(cardTextField)
           // cardNumberTF.becomeFirstResponder()
        }

        if textField.text!.replacingOccurrences(of: " ", with: "").count == 16{
           // cardNumberTF.resignFirstResponder()
        }

    }
//
//    @IBAction func backBtnClickMethod(_ sender: Any) {
//        if(isfromVC != nil && isfromVC){
//            self.navigationController?.popViewController(animated: true)
//        }
//        else{
//            self.slideMenuController()?.toggleLeft()
//        }
//    }
//
//    @IBAction func addNewBtnClickMethod(_ sender: Any) {
//        self.view.endEditing(true)
//        self.checkMandatoryFields()
//        if(checkMandatoryCard){
//            card = STPCardParams();
//            card.number = cardNumberTF.text!.replacingOccurrences(of: " ", with: "")
//            card.expMonth = UInt(mmTF.text!)!
//            card.expYear = UInt(yearTF.text!)!
//            card.cvc = cvvNOTF.text ?? ""
//            self.performStripeOperation()
//        }
//    }
//
//    func paymentCardTextFieldDidChange(_ textField: STPPaymentCardTextField)
//    {
//
//        if(textField.isValid){
//            card = STPCardParams();
//            card.number = textField.cardNumber;
//            card.expMonth = textField.expirationMonth;
//            card.expYear = textField.expirationYear;
//            card.cvc = textField.cvc;
//        }
//    }
//
//    func paymentCardTextFieldDidEndEditingNumber(_ textField: STPPaymentCardTextField) {
//        //if(textField.isValid){
//        uiImage = textField.brandImage
//        //}
//        cardImage.image = textField.brandImage
//
//    }
//
//    func checkMandatoryFields()
//    {
//        let cardno: String? = cardNumberTF.text ?? ""
//        let cardexpMnt = mmTF.text ?? ""
//        let cardexpYr = yearTF.text ?? ""
//        let cardcvc: String? = cvvNOTF.text ?? ""
//        if (cardno?.count == nil || cardno == "")
//        {
//            Globals.showAlert(message: "Enter Card No".localized, controller: self)
//        }
//        else if (cardcvc?.count == nil || cardcvc == "")
//        {
//            Globals.showAlert(message: "Enter CVV".localized, controller: self)
//        }
//        else if ((cardexpMnt == "") || (cardexpYr == ""))
//        {
//            Globals.showAlert(message: "Enter expiry details".localized, controller: self)
//        }
//        else
//        {
//            checkMandatoryCard = true
//        }
//    }
//
//    func performStripeOperation()
//    {
//        Utils.showProgress()
//        let stripeClient = STPAPIClient.init(publishableKey: StripePublishKey)
//        stripeClient.createToken(withCard: card) { (token, error) in
//
//            //SSCommonClass.dismissProgress()
//            if (error != nil)
//            {
//                print(error!)
//                let string  = error?.localizedDescription
//                Globals.showAlert(message: string!, controller: self)
//                Utils.dismissProgress()
//            }
//            else
//            {
//                self.addCardForPayment(token: (token?.tokenId)!)
//
//            }
//        }
//    }
//
//    // MARK: - Webservice Methods
//
//    func addCardForPayment(token:String)
//    {
//        let reachabilityManager             = NetworkReachabilityManager()
//        let reacabilitystatus                   = reachabilityManager?.isReachable
//        if(reacabilitystatus!)
//        {
//            var params:[String:Any]             = [String:Any]()
//            params[kdevice_type]               = DEVICE_TYPE
//            params[klanguage]                    = Language.language.rawValue
//            params[ksession]              = UserDefaultController.sharedInstance.getSessionToken().count > 0 ? UserDefaultController.sharedInstance.getSessionToken() : "235235235"
//            params[kstripe_token] = token
//
//            AGWebServiceManager.sharedInstance.WebServiceAddCardRequest(params: params, success: { (response) in
//                Utils.dismissProgress()
//                let responseDict = response.dictionary
//                let status = responseDict!["response_status"]?.int
//                switch(status)
//                {
//                case 1? :
//                    self.navigationController?.popViewController(animated: true)
//                default:
//                    Globals.showAlert(message: (responseDict!["response_msg"]?.string)!, controller: self)
//                    Utils.dismissProgress()
//                }
//            })
//            { (error) in
//                Utils.dismissProgress()
//            }
//
//        }
//        else
//        {
//            Globals.showAlert(message: notConnetedToNetwork.localized, controller: self)
//        }
//
//    }
//
    func modifyCreditCardString(creditCardString : String) -> String {
        let trimmedString = creditCardString.components(separatedBy: .whitespaces).joined()

        let arrOfCharacters = Array(trimmedString)
        var modifiedCreditCardString = ""

        if(arrOfCharacters.count > 0) {
            for i in 0...arrOfCharacters.count-1 {
                modifiedCreditCardString.append(arrOfCharacters[i])
                if((i+1) % 4 == 0 && i+1 != arrOfCharacters.count){
                    modifiedCreditCardString.append(" ")
                }
            }
        }
        return modifiedCreditCardString
    }
//
}
//extension AddCardVC : UITextFieldDelegate{
//    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
//        
//        
//        let newLength = (textField.text ?? "").count + string.count - range.length
//        if(textField == cardNumberTF) {
//            
//            return newLength <= 19
//        }else if (textField == cvvNOTF){
//            return newLength <= 3
//        }
//        return true
// 
//    }
// 
//}
