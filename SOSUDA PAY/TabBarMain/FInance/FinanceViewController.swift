//
//  FinanceViewController.swift
//  SOSUDA PAY
//
//  Created by Saifur Rahman on 21/03/21.
//

import UIKit
import Stripe
class FinanceViewController: UIViewController{
    @IBOutlet weak var topNavBar: TopHeader!
    @IBOutlet weak var cardsTableview: UITableView!
    var paymentField = STPPaymentCardTextField()
   // let customerContext = STPCustomerContext(keyProvider: MyAPIClient())
    var arrayOfCards = [DataForcard]()
    override func viewDidLoad() {
        super.viewDidLoad()
        cardsTableview.tableViewSetupWithViewC(ViewController: self, cellArrayToRegister: [ShowCardTableViewCell.getCellIdentifier()])
        topNavBar.backButton.setImage(UIImage(systemName: "person.circle.fill"), for: .normal)
        topNavBar.backButtonWidth.constant = 60
        topNavBar.backButton.tintColor = .lightGray
        topNavBar.title.text = "Finance"
        topNavBar.backgroundColor = #colorLiteral(red: 0.1957393885, green: 0.2087394297, blue: 0.7472228408, alpha: 1)
        cardsTableview.delegate = self
        cardsTableview.dataSource = self
       
    }
    override func viewWillAppear(_ animated: Bool) {
        getCards()
    }
    
    func getCards(){
            if  let dict = UserDefaults.standard.value(forKey: "data"){
            let dictMod = try? JSONDecoder().decode(UserDictModel.self, from: dict as! Data)
                var urlString = (STRIPEGETCARDS as NSString).appendingPathComponent("?customerId=\(dictMod!.stripeCustomerID)&limit=\(10)")
                ResponseFromApi.shared.getRequestWithHeader(url: urlString ,header: ["Authorization" : "Bearer \(dictMod!.token)","Content-Type": "application/json"]) { (Response,dat,isSuccess) in
                if isSuccess{
                    print(Response)
                    let dictAccount = try? JSONDecoder().decode(SavedCards.self, from: dat!)
                    self.arrayOfCards = dictAccount!.data
                    self.cardsTableview.reloadData()
                }
            }
            }
    }
    
    @objc func addCardTapped(_ sender:UITapGestureRecognizer){
        let viewCOntroller = self.storyboard?.instantiateViewController(withIdentifier: "AddCardVC") as? AddCardVC
        self.navigationController?.pushViewController(viewCOntroller!, animated: true)
    }
    // MARK: - Navigation
}

extension FinanceViewController:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrayOfCards.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ShowCardTableViewCell") as! ShowCardTableViewCell
        cell.last4digits.text  = self.arrayOfCards[indexPath.row].last4
        cell.expiringDate.text  = "\(self.arrayOfCards[indexPath.row].expMonth)" + "/" + "\(self.arrayOfCards[indexPath.row].expYear)"
        cell.nameLabel.text = "Test User\(indexPath.row)"
        cell.deleteBtn.tag = indexPath.row
        cell.deleteBtn.addTarget(self, action: #selector(deleteCard(_:)), for: .touchUpInside)
        return cell
    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let footview = TopHeader()
        footview.frame.size.height = 70
        footview.backButton.setBackgroundImage(UIImage(systemName: "plus.circle"), for: .normal)
        footview.headerView.backgroundColor = UIColor.white
        footview.backButton.frame.size.width = 60
        footview.backButton.tintColor = #colorLiteral(red: 0.1957393885, green: 0.2087394297, blue: 0.7472228408, alpha: 1)
        footview.title.text  = "Add Card"
        footview.title.textColor = #colorLiteral(red: 0.1957393885, green: 0.2087394297, blue: 0.7472228408, alpha: 1)
        footview.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(addCardTapped(_:))))
        return footview
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
       return 70
    }
    
    @objc func deleteCard(_ sender :UIButton){
        
                if  let dict = UserDefaults.standard.value(forKey: "data"){
                let dictMod = try? JSONDecoder().decode(UserDictModel.self, from: dict as! Data)
                    if Reachability.isConnectedToNetwork(){
                        let carddata = self.arrayOfCards[sender.tag]
                        let urlString = (DELETESTRIPECARD as NSString).appendingPathComponent("?CustomerId=\(dictMod!.stripeCustomerID)&CardId=\(carddata.id)")
                    ResponseFromApi.shared.postRequestWithHeader(url: urlString ,header: ["Authorization" : "Bearer \(dictMod!.token)","Content-Type": "application/json"]) { (Response,dat,isSuccess) in
                    if isSuccess{
                        print(Response)
                    }
                        self.getCards()
                    } }
                }
    }
}

//extension FinanceViewController:STPAddCardViewControllerDelegate,STPPaymentCardTextFieldDelegate {
//
//    func addCardViewControllerDidCancel(_ addCardViewController: STPAddCardViewController) {
//        self.dismiss(animated: true, completion: nil)
//        self.navigationController?.setNavigationBarHidden(true, animated: true)
//
//        self.navigationController?.popViewController(animated: true)
//    }
//
//    func addCardViewController(_ addCardViewController: STPAddCardViewController, didCreatePaymentMethod paymentMethod: STPPaymentMethod, completion: @escaping STPErrorBlock) {
//
//        print( paymentMethod.customerId,paymentMethod)
//
////
//      let c =  paymentMethod as? STPSource
//        c?.stripeID
//        STPAPIClient.shared.createToken(withSSNLast4: (paymentMethod.card?.last4)!) { (tkn, err) in
//            print(tkn)
//        }
//        let cardParams = STPCardParams()
//   //      cardParams.number = paymentMethod.card?.
////             cardParams.expMonth = paymentMethod.card?.expMonth
////             cardParams.expYear = paymentMethod.card?.expYear
////        cardParams.cvc = paymentMethod.card?
////        STPAPIClient.shared.createToken(withCard: paymentMethod.) { (token: STPToken?, error: Error?) in
////                 guard let token = token, error == nil else {
////                     // Present error to user...
////                     return
////                 }
////                 self.dictPayData["stripe_token"] = token.tokenId
////                 print(self.dictPayData)
////
////             }
//
//        self.dismiss(animated: true, completion: nil)
//        self.navigationController?.setNavigationBarHidden(true, animated: true)
//
//        self.navigationController?.popViewController(animated: true)
//    }
//
//    func paymentCardTextFieldDidEndEditingNumber(_ textField: STPPaymentCardTextField) {
//        print("hi ")
//    }
//    func addCardViewController(_ addCardViewController: STPAddCardViewController, didCreateToken token: STPToken, completion: (Error?) -> Void) {
//        print(token)
//    }
//
//}
