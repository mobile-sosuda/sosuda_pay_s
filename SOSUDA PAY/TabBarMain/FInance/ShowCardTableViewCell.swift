//
//  ShowCardTableViewCell.swift
//  SOSUDA PAY
//
//  Created by Saifur Rahman on 25/03/21.
//

import UIKit

class ShowCardTableViewCell: UITableViewCell {

    @IBOutlet weak var deleteBtn: UIButton!
    @IBOutlet weak var cardView: UIView!
    @IBOutlet weak var expiringDate: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var last4digits: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        cardView.layer.cornerRadius = 20
        cardView.dropShadowAccordingly(color: .black, width: 1, height: 1, rad: 2)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
