//
//  CountryTableViewCell.swift
//  SOSUDA PAY
//
//  Created by Saifur Rahman on 10/03/21.
//

import UIKit

class CountryTableViewCell: UITableViewCell {

    @IBOutlet weak var countrynameLabel: UILabel!
    @IBOutlet weak var countryCodeLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
